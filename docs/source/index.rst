.. cardioclean documentation master file, created by
   sphinx-quickstart on Wed Dec  5 23:07:46 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to cardioclean's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

util.py
-------

.. automodule:: util
   :members:


sigbind.py
----------

.. automodule:: sigbind
   :members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
