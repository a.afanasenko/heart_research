#!/usr/bin/anv python
# coding: utf-8

import wfdb
import sys
import numpy as np
import click
import json
from argparse import ArgumentParser
from matplotlib import pyplot as plt
from cardio_clean.sigbind import cardio_filter
from cardio_clean.qrsdetect import *
from cardio_clean.qrsclassify import *
from cardio_clean.wavdetect import find_points
from cardio_clean.util import ecgread


def build_args():
    parser = ArgumentParser()

    parser.add_argument(
        '-t', '--time-range',
        type=int,
        default=6,
        help='Time in seconds to display'
    )

    options, filenames = parser.parse_known_args()
    if not filenames:
        #filenames.append("/Users/arseniy/SERDECH/data/ROXMINE/Rh2004")
        #filenames.append("/Users/arseniy/SERDECH/data/PHYSIONET/I16")
        filenames.append("/Users/arseniy/SERDECH/data/24h/Holter_001")
        #filenames.append("/Users/arseniy/heart-research/cardio-clean/test
        # /TestFromDcm.ecg")
        # 1003 2018

    if not filenames:
        print("At least one input file should be specified")
        sys.exit(1)

    return options, filenames


def process_points(recordname):

    # загрузили сигнал
    sig, hdr = ecgread(recordname)
    fs = hdr["fs"]

    sig = cardio_filter(
        -sig,
        fs=fs,
        bias=hdr["baseline"],
        mains_attenuation=0.05,
        baseline_attenuation=0.01
    )

    metadata = qrs_detection(
        sig,
        fs=fs
    )

    find_points(
        sig,
        fs=fs,
        metadata=metadata,
        bias=hdr["baseline"],
        gain=hdr["adc_gain"]
    )

    print("{} cycles found".format(len(metadata)))

    return metadata


@click.command()
@click.argument("input_file", type=click.Path(exists=True))
@click.argument("output_file", type=click.Path(exists=False))
def main(input_file, output_file):
    with open(output_file, "w") as f:
        metadata = process_points(input_file)
        json.dump(metadata, f)


if __name__ == "__main__":
    main()
