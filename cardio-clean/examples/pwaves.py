# coding: utf-8

import wfdb
from matplotlib import pyplot as plt
import sys
from scipy.signal import hann
import numpy as np
from scipy.fftpack import fft, ifft

def remove_extension(s):
    parts = s.split('.')
    if len(parts) > 1:
        return '.'.join(parts[:-1])
    else:
        return s


def build_comb_filter(fs, n, att, base_freq=50.0, q=5.0):
    """Построение АЧХ гребенчатого режекторного фильтра

    :param fs: частота дискретизации
    :param n: число точек в спектре
    :param att: ослабление гармоник 0 - 1
    :param base_freq: частота первой гармоники
    :param q: ширина полосы задержания
    :return: f_grid, response
    """

    att = min(1.0, max(att, 0.0))
    response = np.ones(n)
    f_grid = np.arange(0.0, fs, float(fs)/n)

    for i, f in enumerate(f_grid):
        real_f = f if f <= fs/2 else fs-f
        for harm in np.arange(base_freq, fs/2, base_freq):
            d = (1.0 - att) * np.exp(-((real_f-harm)/q)**2)
            response[i] = min(response[i], 1.0 - d)

    return f_grid, response


def build_rj_filter(fs, n, att, base_f, q):
    """Построение АЧХ режекторного фильтра

    :param fs: частота дискретизации
    :param n: число точек в спектре
    :param att: ослабление гармоник 0 - 1
    :param base_f: частота первой гармоники
    :param q: ширина полосы задержания
    :return: f_grid, response
    """

    att = min(1.0, max(att, 0.0))
    response = np.ones(n, np.float32)
    f_grid = np.arange(0.0, fs, float(fs)/n)

    for i, f in enumerate(f_grid):
        real_f = f if f <= fs/2 else fs-f
        d = (1.0 - att) * np.exp(-((real_f-base_f)/q)**2)
        response[i] = min(response[i], 1.0 - d)

    return f_grid, response


def build_lp_filter(fs, n, att, freq):
    """Построение АЧХ нч фильтра

    :param fs: частота дискретизации
    :param n: число точек в спектре
    :param att: ослабление гармоник 0 - 1
    :param freq: граница полосы пропускания
    :return: f_grid, response
    """

    att = min(1.0, max(att, 0.0))
    response = np.ones(n, np.float32)
    f_grid = np.arange(0.0, fs, float(fs)/n)

    for i, f in enumerate(f_grid):

        real_f = f if f <= fs/2 else fs-f

        if 0 < real_f <= freq[0]:
            response[i] = 1.0
        elif freq[0] < real_f <= freq[1]:
            k = (real_f - freq[0]) / (freq[1] - freq[0])
            response[i] = 1.0 - k*(1.0 - att)
        else:
            response[i] = att

    return f_grid, response


def cardio_filter(sig, fs):
    """Частотная фильтрация кардиосигнала

    :param sig: массив отсчетов входного сигнала
    :param fs: частота дискретизации в Гц
    :param bias: значение неизменной постоянной составляющей
    :param mains_attenuation: коэффициент ослабления гармоник сети(0 - полное
    подавление)
    :param baseline_attenuation: коэффициент ослабления дрейфа базовой линии (
    0 - полное подавление)
    :keyword fix_nan: заменять Nan и Inf во входном сигнале
    :keyword mains: частота сети
    :keyword aperture: апертура БПФ
    :return: массив отсчетов выходного сигнала
    """

    # размер окна БПФ
    aperture = 512
    baseline_attenuation = 0.01  #коэффициент ослабления дрейфа базовой линии (
    # 0 - полное подавление)
    highfreq_attenuation = 0.01  # коэффициент ослабления высоких частот (
    # 0 - полное подавление)

    f_grid, bsl_response = build_rj_filter(
        fs=fs,
        n=aperture,
        att=baseline_attenuation,
        base_f=0.0,
        q=1.0
    )

    fft_response = bsl_response

    # частота
    highfreq_cutoff = [15.0, 30.0]
    f_grid, lpf_response = build_lp_filter(
        fs=fs,
        n=aperture,
        att=highfreq_attenuation,
        freq=highfreq_cutoff
    )

    fft_response *= lpf_response

    y = np.zeros(len(sig), np.float32)
    hamwnd = np.array(hann(aperture))
    step = int(aperture / 2)

    ham_left = hamwnd.copy()
    ham_left[:step+1] = np.max(hamwnd)

    n2 = len(sig) - aperture
    for n1 in xrange(0, n2, step):
        # для ослабления краевых эффектов берем несимметричное окно в
        # начале
        wnd = ham_left if n1 == 0 else hamwnd
        # комплексный спектр с учетом окна и смещения
        xf = fft(wnd * (np.array(sig[n1:n1 + aperture])))
        # отфильтрованный сигнал в окне
        yt = np.real(ifft(xf * fft_response))

        y[n1:n1 + aperture] += yt

    return y


if __name__ == '__main__':

    filename = remove_extension(sys.argv[1])

    channel = int(sys.argv[2]) - 1

    data, fields = wfdb.rdsamp(filename)
    fs = fields["fs"]

    numch = data.shape[1]

    if channel >= numch:
        print('В записи {} каналов'.format(numch))
        sys.exit(1)

    raw_signal = data[:,channel]
    raw_signal -= np.mean(raw_signal)
    sig = cardio_filter(raw_signal, fs)

    fig, ax = plt.subplots(2, 1, sharex="col")

    ax[0].plot(raw_signal)
    ax[1].plot(sig)

    # 2 секунды
    plt.xlim([0, 2*fields["fs"]])

    # 2 милливольта
    ax[0].set_ylim([-0.5, 2])
    ax[1].set_ylim([-0.5, 2])

    plt.title(u'Отведение № {}'.format(channel+1))

    plt.show()

