# coding: utf-8

import time
import json
import sys
import numpy as np
from matplotlib import pyplot as plt

from cardio_clean.qrsclassify import density_classifier
from cardio_clean.util import ecgread
from cardio_clean.metadata import extract_cycle
from cardio_clean.sigbind import cardio_filter
from memory_profiler import memory_usage
from cardio_clean.data_keys import QrsKeys as QK


def show_cycle_group(sig, fs, metadata, groupdesc):

    n = min(20, groupdesc["count"])

    samples = [qrs for qrs in metadata if qrs[QK.qrs_class_id] == groupdesc[
        "id"]]

    chan = 1

    plt.clf()
    for i in xrange(n):
        y, origin = extract_cycle(sig, fs, samples[i])
        x = np.arange(len(y)) - origin
        plt.plot(x, y[:,chan], "c" if i<n-1 else "k")

    plt.title("Group: {}{:03d} [{}]".format(
        groupdesc["type"],
        groupdesc["id"],
        groupdesc["count"]
    ))
    plt.show()

def main():

    #filename = "/Users/arseniy/SERDECH/data/PHYSIONET/I60"
    #filename = "/Users/arseniy/SERDECH/data/PHYSIONET/I24.json"
    filename = "/Users/arseniy/SERDECH/data/24h/Holter_002"
    #filename = "/Users/arseniy//Downloads/Test20191007.ecg.json"

    metaname = filename + ".json"

    print("Load...")

    sig, header = ecgread(filename)

    print("Filter...")

    sig = cardio_filter(
        sig,
        fs=header["fs"],
        bias=header["baseline"],
        mains_attenuation=0.05,
        baseline_attenuation=0.01,
    )

    metadata = json.load(open(metaname))
    print("{} cycles".format(len(metadata)))
    print("Start...")

    ts = time.clock()
    clusters = density_classifier(sig, header, metadata, class_generation=0.7)
    print(" {:.2f} s".format(time.clock() - ts))

    for i, clust in enumerate(clusters):
        print("Show next {}/{}, {}? (y[es]/n[o]/q[uit])".format(i+1,
                                                             len(clusters),
                                               clust["type"]))
        c = sys.stdin.readline().strip()
        if c == "q":
            break
        elif c == "y":
            show_cycle_group(sig, header["fs"], metadata, clust)


if __name__ == "__main__":
    main()
    #mem = max(memory_usage(proc=main()))
    #print("Maximum memory used: {0} MiB".format(str(mem)))
