# coding: utf-8


from matplotlib import pyplot as plt
import json

from cardio_clean.turbulence import turbulence_analyse

if __name__ == "__main__":

    #metaname = "/Users/arseniy/SERDECH/data/PHYSIONET/I24.json"
    metaname = "/Users/arseniy/SERDECH/data/24h/Holter_001.json"
    #metaname = "/Users/arseniy//Downloads/Test20191007.ecg.json"

    print("Load...")
    meta = json.load(open(metaname))
    print("{} cycles".format(len(meta)))
    print("Start...")
    turb_data, trend_data = turbulence_analyse(meta)

    print([x["qrs_index"] for x in turb_data])
    print("TO={}, TS={}".format(trend_data["TO"], trend_data["TS"]))

    x0 = trend_data["start_index"]
    x1 = x0 + len(trend_data["curve"])

    xx = [i for i in range(x0, x1)]
    alpha = max(0.1, 1.0/len(turb_data))
    for turb in turb_data:
        plt.plot(xx, turb["curve"], 'b', alpha=alpha)

    #print(json.dumps(turb_data[0:2], indent=1))

    if trend_data["curve"] is not None:
        plt.plot(xx, trend_data["curve"], "r")

    plt.xticks(xx)

    plt.show()