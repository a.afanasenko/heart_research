# coding: utf-8

import time
import sys
import json
from matplotlib import pyplot as plt

from cardio_clean.wavdetect import find_points
from cardio_clean.arrythmia import *


def scatterogram(series, ax):
    for i, x in enumerate(series):
        if i:
            ax.scatter(series[i - 1], x, c="k")


def show_qt(metadata):
    qt_int = []
    qtc_int = []
    rr_int = []

    for ncycle, qrs in enumerate(metadata):

        # if is_artifact(qrs):
        #    continue

        if qrs["qt_duration"] is not None:
            qt_int.append(qrs["qt_duration"])

        if qrs["qtc_duration"] is not None:
            qtc_int.append(qrs["qtc_duration"])

        if qrs["RR"] is not None:
            rr_int.append(qrs["RR"])

    fig, axarr = plt.subplots(2, 2)

    axarr[0, 0].set_title("QT")
    scatterogram(qt_int, axarr[0, 0])
    axarr[0, 1].set_title("QTc")
    scatterogram(qtc_int, axarr[0, 1])
    axarr[1, 0].set_title("RR")
    scatterogram(rr_int, axarr[1, 0])

    plt.show()


if __name__ == "__main__":

    if len(sys.argv) > 1:
        with open(sys.argv[1], "r") as fj:
            ry = json.load(fj)
    else:
        # filename = "/Users/arseniy/SERDECH/data/PHYSIONET/I60"
        # metaname = "/Users/arseniy/SERDECH/data/PHYSIONET/I24.json"
        metaname = "/Users/arseniy/SERDECH/data/24h/Holter_002.json"
        # metaname = "/Users/arseniy//Downloads/Test20191007.ecg.json"

        print("Load...")
        metadata = json.load(open(metaname))
        print("{} cycles".format(len(metadata)))
        print("Start...")
        ts = time.clock()
        ry = define_rythm(metadata)
        print(" {:.2f} s".format(time.clock() - ts))

    pvc_overlap = {}

    # Расчет статистики нарушений ритма
    rtm = {}
    for rythm_ep in ry:

        desc = rythm_ep["desc"]

        if desc.startswith("PVC_"):
            res = rythm_ep["start"]
            if res in pvc_overlap:
                print("duplicate start time")
            else:
                pvc_overlap[res] = rythm_ep["end"]

        if desc not in rtm:
            rtm[desc] = {"count": 0, "max_len": 0, "total_len": 0, "ref": 0}

        rtm[desc]["count"] += 1
        dur = rythm_ep["end"] - rythm_ep["start"]
        if dur > rtm[desc]["max_len"]:
            rtm[desc]["max_len"] = dur
            rtm[desc]["ref"] = rythm_ep["start"]

        rtm[desc]["total_len"] += dur

    for k, v in sorted(rtm.items(), key=lambda x: x[1]["total_len"],
                       reverse=True):
        print(u"{}:\t'эпизодов {:5d},\tобщ. {}\tмакс. {}\tсм. "
              u"{:.1f}".format(
            k,
            v["count"],
            time.strftime("%H:%M:%S", time.gmtime(v["total_len"])),
            time.strftime("%H:%M:%S", time.gmtime(v["max_len"])),
            v["ref"]
        ))

    pov = [(k, v) for k, v in sorted(pvc_overlap.items(), key=lambda x: x[0])]
    for i, se in enumerate(pov):
        if i > 0 and pov[i - 1][1] > se[0]:
            print(i, pov[i - 1], se)

    j = 0
    for rythm_ep in ry:
        if rythm_ep["desc"] == "AVB_II":
            j += 1
            print("AVBII", rythm_ep["start"])
            if j > 10:
                break
