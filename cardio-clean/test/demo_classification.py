#!/usr/bin/anv python
# coding: utf-8

import wfdb
import sys
import numpy as np

from argparse import ArgumentParser
from matplotlib import pyplot as plt
from cardio_clean.sigbind import cardio_filter
from cardio_clean.qrsdetect import *
from cardio_clean.qrsclassify import *
from cardio_clean.wavdetect import find_points
from cardio_clean.util import ecgread


def build_args():
    parser = ArgumentParser()

    parser.add_argument(
        '-t', '--time-range',
        type=int,
        default=6,
        help='Time in seconds to display'
    )

    options, filenames = parser.parse_known_args()
    if not filenames:
        #filenames.append("/Users/arseniy/SERDECH/data/ROXMINE/Rh2004")
        #filenames.append("/Users/arseniy/SERDECH/data/PHYSIONET/I16")
        filenames.append("/Users/arseniy/SERDECH/data/24h/Holter_001")
        #filenames.append("/Users/arseniy/heart-research/cardio-clean/test
        # /TestFromDcm.ecg")
        # 1003 2018

    if not filenames:
        print("At least one input file should be specified")
        sys.exit(1)

    return options, filenames


def get_qrsclass(recordname):

    # загрузили сигнал
    sig, hdr = ecgread(recordname)
    fs = hdr["fs"]

    sig = signal_slice(sig, 0, 1000000)

    sig = cardio_filter(
        -sig,
        fs=fs,
        bias=hdr["baseline"],
        mains_attenuation=0.05,
        baseline_attenuation=0.01
    )

    metadata = qrs_detection(
        sig,
        fs=fs
    )

    find_points(
        sig,
        fs=fs,
        metadata=metadata,
        bias=hdr["baseline"],
        gain=hdr["adc_gain"]
    )

    qrs_classes = incremental_classifier(
        sig,
        hdr,
        metadata,
        classgen_t=0.99
    )

    print("{} cycles found".format(len(metadata)))
    print("{} classes detected".format(len(qrs_classes)))

    fig, axarr = plt.subplots(hdr["channels"], 1, sharex="col")

    for class_id, qcl in enumerate(
            sorted(
                qrs_classes,
                key=lambda x: x["count"],
                reverse=True
            )):

        # расчет временных точек
        samples = qcl["average"].shape[0]
        t = np.linspace(0.0, 1000.0*samples/fs, samples)

        group = [n for n,x in enumerate(metadata) if x[QK.qrs_class_id] == class_id]
        group = group[:20]


        for qrsnum in group:
            l, r, c = get_qrs_bounds(metadata[qrsnum], fs, 1)
            xpos = np.arange(l-c,r-c)
            for chan in range(sig.shape[1]):
                axarr[chan].plot(xpos, sig[l:r,chan], alpha=0.3)

        # построение усредненного комплекса
        #for chan, wave in signal_channels(qcl["average"]):
#
        #    axarr[chan].plot(t, wave)
        #    axarr[chan].grid(True)
#
        #    if chan == 0:
        #        axarr[0].set_title("[{}]".format(qcl["count"]))
        #        axarr[-1].set_xlabel("t, ms")

        plt.show()
        print("Show next class (y/n)?")
        if sys.stdin.read(1).strip() == "n":
            break


def main():
    options, filenames = build_args()

    get_qrsclass(filenames[0])


if __name__ == "__main__":
    main()