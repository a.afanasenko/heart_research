# coding: utf-8

import cProfile as profile
import pstats

from cardio_clean.sigbind import cardio_filter
from cardio_clean.wavdetect import find_points
from cardio_clean.qrsdetect import qrs_detection
from cardio_clean.qrsclassify import incremental_classifier
from cardio_clean.metadata import metadata_postprocessing
from cardio_clean.arrythmia import define_rythm
from cardio_clean.util import ecgread


print('start cardio_filter')

pr = profile.Profile()

sig, header = ecgread("/Users/arseniy/SERDECH/data/24h/Holter_24h")

sig = cardio_filter(
    sig,
    fs=header["fs"],
    bias=header["baseline"],
    mains_attenuation=0.05,
    baseline_attenuation=0.01
)

print('start qrs_detection')

metadata = qrs_detection(sig,
                         fs=header["fs"],
                         minqrs_ms=20)
#pr.enable()
print('start find_points')
find_points(sig,
            fs=header["fs"],
            metadata=metadata,
            bias=header["baseline"],
            gain=header["adc_gain"],
            debug=False)
#pr.disable()

print('start metadata_postprocessing')
#pr.enable()
metadata_postprocessing(metadata,
                        sig,
                        header)
#pr.disable()
print('start incremental_classifier')

qrs_classes = incremental_classifier(
    sig,
    header,
    metadata
)

print('start define_rythm')

pr.enable()
rythms = define_rythm(metadata)
pr.disable()

pr.dump_stats('profile.pstat')

# Выводим 10 самых затратных функций
p = pstats.Stats('profile.pstat')
p.sort_stats('time').print_stats(10)

