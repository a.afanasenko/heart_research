# coding: utf-8

import time
import sys
import os
import json

from matplotlib import pyplot as plt
from scipy.signal import hann
from scipy.fftpack import fft

from cardio_clean.metadata import *
from cardio_clean.wavdetect import ddwt, find_points, explorer
from cardio_clean.qrsclassify import density_classifier
from cardio_clean.arrythmia import *
from cardio_clean.ishemia import define_ishemia_episodes
from cardio_clean.pmdetect import define_pacemaker_episodes

from cardio_clean.sigbind import cardio_filter, zcfind
from cardio_clean.qrsdetect import qrs_detection, qrs_detection_dev
from cardio_clean.util import ecgread, signal_channels, signal_slice
from cardio_clean.turbulence import turbulence_analyse

from memory_profiler import memory_usage
from cardio_clean.data_keys import QrsKeys as QK
from cardio_clean.interval_timer import IntervalTimer
from cardio_clean.statvariation import rhythm_stats


def dummy_shift(x, n):
    return np.concatenate((x[n:], np.ones(n)*x[-1]))


def multiplot(siglist):
    fig, axarr = plt.subplots(len(siglist), 1, sharex="col")
    for i, s in enumerate(siglist):
        axarr[i].plot(s)
        axarr[i].grid()
    print("Look at the plots")
    plt.show()


def multispectrum(siglist):
    fig, axarr = plt.subplots(len(siglist), 1, sharex="col")
    for i, s in enumerate(siglist):
        n1 = len(s)
        n2 = int(n1/2)
        wgt = np.array(hann(n1))
        yf = fft(s * wgt)
        yf = np.abs(yf[0:n2])
        axarr[i].plot(yf)
        axarr[i].grid()
    print("Look at the plots")
    plt.show()


def multilinespec(siglist):
    leg = []
    for i, s in enumerate(siglist):
        n1 = len(s)
        n2 = int(n1/2)
        wgt = np.array(hann(n1))
        yf = fft(s * wgt)
        yf = np.abs(yf[0:n2])
        plt.plot(yf)
        leg.append(str(i))
    plt.grid()
    plt.legend(leg)
    print("Look at the plots")
    plt.show()


def show_filter_responses(spectral=False):

    # пропускаем дельта-функцю через банк фильтров
    T = 256
    x = np.zeros(T, float)
    mid = int(T/2)
    x[mid] = 1.0
    app, ders = ddwt(x, num_scales=5)

    if spectral:
        multilinespec(ders[1:])
    else:
        multiplot(ders)
        for i, x in enumerate(ders[1:]):
            dt_theor = (2.0**i - 1)/2
            dt_exrep = zcfind(x) - mid
            print(dt_theor, dt_exrep)


def show_decomposition(filename, chan, scale=4, sec_from=0, sec_to=0):

    sig, header = ecgread(filename)
    fs = header["fs"]

    smp_from = int(sec_from * fs)

    if sec_to:
        smp_to = min(int(sec_to * fs), sig.shape[0])
    else:
        smp_to = sig.shape[0]

    s = sig[smp_from:smp_to, chan]

    app, ders, noise = ddwt(s, num_scales=scale, store_detail={0,1,2,3,4})
    print(noise)
    #ders[0] = s
    ders = ders[1:]
    app = app[1:]

    #t = np.arange(0, float(s.shape[0])/fs, 1.0/fs)
    t = np.arange(0, s.shape[0])

    fig, axarr = plt.subplots(1, len(ders), sharex="row")
    for i, s in enumerate(ders):
        axarr[i].plot(t, s, "k")
        if len(app[i]) > 0:
            axarr[i].plot(t, app[i],"b")
        axarr[i].grid()
        axarr[i].set_ylabel(str(i))
    print("Look at the plots")
    plt.show()


def show_extrema(filename, chan, scale=4, sec_from=0, sec_to=0):

    sig, header = ecgread(filename)
    fs = header["fs"]

    smp_from = int(sec_from*fs)

    if sec_to:
        smp_to = min(int(sec_to*fs), sig.shape[0])
    else:
        smp_to = sig.shape[0]

    #sig = cardio_filter(
    #    signal_slice(sig, smp_from, smp_to),
    #    fs=fs,
    #    bias=header["baseline"],
    #    mains_attenuation=0.05,
    #    baseline_attenuation=0.01
    #)

    explorer(signal_slice(sig, smp_from, smp_to), fs=fs, bias=header["baseline"])
    #explorer(sig, fs=fs, bias=header["baseline"])


def show_raw(filename, chanstd, sec_from=0, sec_to=0, filt=True):

    sig, header = ecgread(filename)
    fs = header["fs"]

    smp_from = int(sec_from * fs)

    if sec_to:
        smp_to = min(int(sec_to * fs), sig.shape[0])
    else:
        smp_to = sig.shape[0]

    sig = sig[smp_from:smp_to,:]
    print("min max mean")
    print(np.min(sig), np.max(sig), np.mean(sig))

    if filt:
        sig = cardio_filter(
            sig,
            fs=fs,
            bias=header["baseline"],
            mains_attenuation=0.05,
            baseline_attenuation=0.01,
        )
    else:
        for i, sch in signal_channels(sig):
            n = len(sch)
            nn = len([x for x in sch if np.isnan(x)])
            print("channel {}, len {}, nans {}, mean {}".format(
                i,
                n,
                nn,
                sum(sch) / n
            ))

    nrows = min(6, sig.shape[1])

    fig, axarr = plt.subplots(nrows, 1, sharex="col")

    t = np.arange(0,float(sig.shape[0])/fs,1.0/fs)

    for i, s in signal_channels(sig):

        if i >= nrows:
            break

        axarr[i].plot(t,s)
        axarr[i].grid()
        axarr[i].set_ylabel(chanstd[i])

    print("Look at the plots")
    plt.show()


def print_summary(metadata, chan):
    classes = {}
    classids = {}
    st_intervals = []
    qt_intervals = []
    rr_intervals = []
    wcount = {w: 0 for w in ("p", "q", "r", "s", "t", "r'", "s'")}
    pvc_count = 0
    pvcv_count = 0
    flutter_count = 0

    p_amp = []
    r_amp = []
    t_amp = []

    for ncycle, qrs in enumerate(metadata):

        classletter = qrs[QK.complex_type]
        classes[classletter] = classes.get(classletter, 0) + 1

        classid = qrs[QK.qrs_class_id]
        classids[classid] = classids.get(classid, 0) + 1

        if qrs[QK.p_pos][chan] is not None:
            wcount["p"] += 1
        if qrs[QK.q_pos][chan] is not None:
            wcount["q"] += 1
        if qrs[QK.r_pos][chan] is not None:
            wcount["r"] += 1
        if qrs[QK.s_pos][chan] is not None:
            wcount["s"] += 1
        if qrs[QK.r2_pos][chan] is not None:
            wcount["r'"] += 1
        if qrs[QK.s2_pos][chan] is not None:
            wcount["s'"] += 1
        if qrs[QK.t_pos][chan] is not None:
            wcount["t"] += 1

        if qrs[QK.st_duration][chan] is not None:
            st_intervals.append(qrs[QK.st_duration][chan])

        if qrs[QK.RR] is not None:
            rr_intervals.append(qrs[QK.RR])

        if qrs[QK.qt_duration][chan] is not None:
            qt_intervals.append(qrs[QK.qt_duration][chan])

        if is_pvc(qrs):
            pvc_count += 1
            if is_ve(qrs):
                pvcv_count += 1

        if qrs[QK.p_height][chan] is not None:
            p_amp.append(qrs[QK.p_height][chan])

        if qrs[QK.r_height][chan] is not None:
            r_amp.append(qrs[QK.r_height][chan])

        if qrs[QK.t_height][chan] is not None:
            t_amp.append(qrs[QK.t_height][chan])

        if is_flutter(qrs):
            flutter_count += 1

    print("Комплексы: {}".format(len(metadata)))
    print("Зубцы: {}".format(wcount))

    print("Типы комплексов: {}".format(classes))
    print("Экстрасистолы: {}, ЖЭ: {}".format(pvc_count, pvcv_count))
    print("Циклы с трепетаниями: {}".format(flutter_count))

    if len(classids) < 10:
        print("Автоклассы:")
        print(classids)

    print("ST-интервалы: {}, {}".format(
        len(st_intervals), "в среднем {:.2f} мс".format(np.mean(st_intervals))
        if st_intervals else "-"
    ))

    print("QT-интервалы: {}, {}".format(
        len(qt_intervals), "в среднем {:.2f} мс".format(1000 * np.mean(
        qt_intervals)) if qt_intervals else
        "-"
    ))

    print("RR-интервалы: {}, {}".format(
        len(rr_intervals), "в среднем {:.2f} мс".format(1000 * np.mean(
        rr_intervals)) if rr_intervals else
        "-"
    ))

    if p_amp:
        print("Средняя высота P-зубца {:.2f} мВ".format(
            np.mean(p_amp)
        ))

    if t_amp:
        print("Средняя высота T-зубца {:.2f} мВ".format(
            np.mean(t_amp)
        ))

    if r_amp:
        print("Средняя высота R-зубца {:.2f} мВ".format(
            np.mean(r_amp)
        ))

        print("Высота R-зубца {:.2f}...{:.2f} мВ".format(
            np.min(r_amp), np.max(r_amp)
        ))


def show_qrs(filename, chan, sec_from, sec_to):
    sig, header = ecgread(filename)
    fs = header["fs"]

    smp_from = int(sec_from*fs)

    if sec_to:
        smp_to = min(int(sec_to*fs), sig.shape[0])
    else:
        smp_to = sig.shape[0]

    sigm = cardio_filter(
        signal_slice(sig, smp_from, smp_to),
        fs=fs,
        bias=header["baseline"],
        mains_attenuation=0.05,
        baseline_attenuation=0.01
    )

    metadata, pp = qrs_detection_dev(
        sigm,
        fs=header["fs"]
    )
    print(len(metadata))

    numch = sigm.shape[1]

    fig, axarr = plt.subplots(numch + 1, 1, sharex="col")
    for ch in range(numch):
        axarr[ch].plot(sigm[:,ch],"b")

        for ncycle, qrs in enumerate(metadata):
            lb = int(qrs[QK.qrs_start] * fs)
            rb = int(qrs[QK.qrs_end] * fs)
            axarr[ch].plot(np.arange(lb, rb), sigm[lb:rb,ch], "r")
        axarr[ch].grid()
    axarr[numch].plot(pp, "b")
    axarr[numch].grid(True)

    print("Look at the plots")
    plt.show()


def show_classes(qrsclasses):

    n = 10
    for c in sorted(qrsclasses, key=lambda x: x["count"], reverse=True):
        print("{} {} {} {}".format(c["id"], c["count"], c["type"],
                                   c["average"].shape))
        n -= 1
        if not n:
            break

        #plt.plot(c["average"][:,1])

    #print("Look at the classes")
    #plt.show()


def show_waves(filename, channel, sec_from=0, sec_to=0,
               draw_channel=None):
    sig, header = ecgread(filename)

    if channel is None:
        pass
    else:
        sig = sig[:, channel]
        if draw_channel is not None:
            draw_channel = channel

    print(sig.shape)
    fs = header["fs"]
    print("fs = {} Hz".format(fs))

    smp_from = int(sec_from*fs)

    if sec_to:
        smp_to = min(int(sec_to*fs), sig.shape[0])
    else:
        smp_to = sig.shape[0]

    #plt.plot(sig[smp_from:smp_to, draw_channel],"g")
    #print(sig.dtype)
    #print(header["baseline"])

    print("cardio_filter..."),
    ts = time.clock()
    sig = cardio_filter(
        sig,
        fs=fs,
        bias=header["baseline"],
        mains_attenuation=0.05,
        baseline_attenuation=0.01
    )
    print(" {:.2f} s".format(time.clock() - ts))

    print("qrs_detection..."),
    ts = time.clock()
    metadata = qrs_detection(
        signal_slice(sig, smp_from, smp_to),
        fs=header["fs"]
    )
    print(" {:.2f} s".format(time.clock() - ts))

    print("beats found: {}".format(len(metadata)))

    print("find_points..."),
    ts = time.clock()
    find_points(
        signal_slice(sig, smp_from, smp_to),
        fs=header["fs"],
        bias=header["baseline"],
        gain=header["adc_gain"],
        metadata=metadata,
        pma_detection_on=False
    )
    print(" {:.2f} s".format(time.clock() - ts))

    print("metadata_postprocessing..."),
    ts = time.clock()
    metadata_postprocessing(
        metadata,
        signal_slice(sig, smp_from, smp_to),
        header
    )
    print(" {:.2f} s".format(
        time.clock() - ts))


    #qrslen = [x[QK.qrs_end] - x[QK.qrs_start] for x in metadata]
    #print(min(qrslen), max(qrslen))
    #plt.hist(qrslen, bins=50)
    #plt.show()
    #return

    print("density_classifier..."),
    ts = time.clock()
    qrs_classes = density_classifier(
        signal_slice(sig, smp_from, smp_to),
        header,
        metadata,
        class_generation=0.9
    )
    print(" {:.2f} s".format(time.clock() - ts))
    print("{} groups found".format(len(qrs_classes)))

    nega = [x[QK.qrs_class_id] for x in metadata if x[QK.qrs_class_id] < 0
            and x[QK.qrs_class_id] is not None]
    print("################################")
    print(min(nega) if nega else "-", max(nega) if nega else "-", len(nega))

    show_classes(qrs_classes)

    chss = [x[QK.heartrate] for x in metadata if x[QK.heartrate] is not None]
    if chss:
        print(u"ЧСС: мин. {:.2f}, макс. {:.2f}".format(min(chss), max(chss)))

    missing_hrt = [i for i,x in enumerate(metadata) if x[QK.heartrate] is None]
    if missing_hrt:
        print("Heartrate missing in {} beats".format(len(missing_hrt)))

    art = [i for i,x in enumerate(metadata) if is_artifact(x)]
    if art:
        print(u"Артефакты: {}".format(len(art)))

    print(u"Ритмы..."),
    ts = time.clock()
    ry = define_rythm(metadata)
    print(" {:.2f} s".format(time.clock() - ts))

    junk = json.dumps(ry)
    #with open(filename + ".json", "w") as fj:
    #    json.dump(metadata, fj)

    print("### SUMMARY ###")
    if draw_channel is not None:

        if channel is None:
            s = sig[smp_from:smp_to, draw_channel]
        else:
            s = sig[smp_from:smp_to]

        print(u"Общая длительность: {} c".format(len(s) / fs))
        plt.plot(s, "b")

        # Цвета для раскрашивания зубцов на графике
        pt_keys = {
            QK.q_pos: "g",
            QK.r_pos: "r",
            QK.r2_pos: "m",
            QK.s_pos: "b",
            QK.s2_pos: "c",
            QK.p_pos: "y",
            QK.t_pos: "k",
            QK.t_start: "k",
            QK.t_end: "k"}

        for ncycle, qrs in enumerate(metadata):

            plt.text(
                qrs[QK.qrs_start]*fs,
                np.min(s),
                "%d: %.3f" % (ncycle, 0 if qrs[QK.RR] is None else qrs[QK.RR])
            )

            if is_artifact(qrs):
                continue

            lb = int(qrs[QK.qrs_start] * fs)
            rb = int(qrs[QK.qrs_end] * fs)
            iso = qrs[QK.isolevel][draw_channel] * header["adc_gain"][draw_channel] + header[
                "baseline"][draw_channel]
            plt.plot([lb, rb], [iso]*2, "g:")

            for k in pt_keys:
                point = qrs[k][draw_channel]
                if point is not None:
                    plt.scatter(point, s[point], c=pt_keys[k])

            lb = qrs[QK.p_start][draw_channel]
            rb = qrs[QK.p_end][draw_channel]
            if all((lb, rb)):
                plt.plot(np.arange(lb, rb), s[lb:rb], "y")

            lb = qrs[QK.t_start][draw_channel]
            rb = qrs[QK.t_end][draw_channel]
            if all((lb, rb)):
                plt.plot(np.arange(lb, rb), s[lb:rb], "g")

            lb = qrs[QK.st_start][draw_channel]
            rb = qrs[QK.st_end][draw_channel]
            if all((lb, rb)):
                plt.plot(np.arange(lb, rb), s[lb:rb], "r")

            if is_pvc(qrs):

                px = qrs[QK.r_pos][draw_channel]
                if px is None:
                    px = qrs[QK.qrs_center] * fs

                py = qrs[QK.r_height][draw_channel]
                if py is None:
                    py = 1

                plt.text(
                    px,
                    py,
                    qrs[QK.complex_type],
                    color="r"
                )

    rtm = {}
    for r in ry:
        desc = r["desc"]
        if desc not in rtm:
            rtm[desc] = {"count": 0, "max": 0}

        rtm[desc]["count"] += 1
        rtm[desc]["max"] = max(rtm[desc]["max"], r["end"]-r["start"])

    print(json.dumps(rtm, indent=1, sort_keys=True))

    print(u"Ишемия..."),
    ts = time.clock()
    m = define_ishemia_episodes(signal_slice(sig, smp_from, smp_to), header,
                                metadata)
    print(" {:.2f} s".format(time.clock() - ts))

    # проверка на возможность перевода в JSON
    junk = json.dumps(m)

    if m:
        #print(m)
        print(u"Число эпизодов: {}".format(len(m)))
    else:
        print(u"Ишемия на обнаружена")

    print(u"Статистическая вариабельность..."),
    ts = time.clock()
    stat_vals = rhythm_stats(metadata)
    print(" {:.2f} s".format(time.clock() - ts))
    print(json.dumps(stat_vals, indent=1))

    print(u"Стимулятор..."),
    ts = time.clock()
    m = define_pacemaker_episodes(metadata)
    print(" {:.2f} s".format(time.clock() - ts))

    if m:
        print(m)
        print(u"Число эпизодов: {}".format(len(m)))
    else:
        print(u"Стимулятор не обнаружен")

    print(u"Турбулентность..."),
    ts = time.clock()
    turb_data, trend_data = turbulence_analyse(metadata)
    print(" {:.2f} s".format(time.clock() - ts))

    print("N={}, TO={}, TS={}".format(len(turb_data), trend_data["TO"],
                                          trend_data["TS"]))

    print_summary(metadata, 1 if channel is None else 0)

    if draw_channel is not None:
        plt.grid(True)
        plt.show()

    return metadata


def show_qt_hist(ax, metadata, key):
    numch = len(metadata[0][QK.r_pos])
    for chan in range(numch):
        hdqt = calculate_histogram(metadata, key, channel=chan, bins=10,
                                   censoring=False)
        x = [hdqt[0]["bin_left"]*1000]
        y = [0]
        for bin in hdqt:
            x.append(bin["bin_right"]*1000)
            y.append(bin["percent"])

        x.append(hdqt[-1]["bin_right"] * 1000)
        y.append(0)

        ax.step(x,y)
        ax.set_title("{}".format(sum(x["count"] for x in hdqt)))


def main(op, filename=None):
    # Rh2022 = qr, noise
    # Rh2021 - Rs, extracyc
    # Rh2024 - p(q)RsT
    # Rh2025 = rs
    # Rh2010 - дрейф, шум, артефакты
    # 2004 av block

    if filename is None:
        #filename = "/Users/arseniy/SERDECH/data/PHYSIONET/I24"
        #filename = "/Users/arseniy/SERDECH/data/PHYSIONET/222"
        #filename = "/Users/arseniy/SERDECH/data/PHYSIONET/104"
        #filename = "/Users/arseniy/SERDECH/data/PHYSIONET/I16"
        #filename = "/Users/arseniy/SERDECH/data/PHYSIONET/I59"
        filename = "/Users/arseniy/SERDECH/data/PHYSIONET/I11"
        #filename = "/Users/arseniy/SERDECH/data/PHYSIONET/I15"
        #filename = "/Users/arseniy/SERDECH/data/th-0002"
        #filename = "testI59.ecg"
        #filename = "TestFromDcm.ecg"
        #filename = "TestFindPoint.ecg"
        #filename = "/Users/arseniy/SERDECH/data/Holter_24h"
        #filename = "/Users/arseniy/Downloads/Test20191007.ecg"
        #filename = "/Users/arseniy/SERDECH/data/ROXMINE/I16/I16.ecg"
        #filename = "/Users/arseniy/SERDECH/data/ROXMINE2/pat00022.edf"
        #filename = "/Users/arseniy/SERDECH/data/24h/Holter_004"

    if not (filename.endswith(".ecg") or
                    filename.endswith(".edf") or
                    os.path.isfile(filename + ".hea")):
        print("Файл не подходит или отсутствует")
        return

    sec_from = 0
    sec_to = 100

    if op == "filt":
        show_filter_responses()

    elif op == "raw":
        show_raw(
            filename,
            chanstd=config.CHANNEL_SEQ["easi"],
            sec_from=sec_from,
            sec_to=sec_to,
            filt=True
        )

    elif op == "dwt":
        show_decomposition(
            filename,
            chan=0,
            scale=4,
            sec_from=sec_from,
            sec_to=sec_to,
        )

    elif op == "exp":
        show_extrema(
            filename,
            chan=0,
            scale=4,
            sec_from=sec_from,
            sec_to=sec_to,
        )

    elif op == "qrs":
        show_qrs(
            filename,
            chan=0,
            sec_from=sec_from,
            sec_to=sec_to
        )

    elif op == "waves":
        mdmp = show_waves(
            filename,
            channel=None,
            draw_channel=1,
            sec_from=sec_from,
            sec_to=sec_to,
        )

    elif op == "full":
        metadump = show_waves(
            filename,
            channel=None,
            draw_channel=None,
            sec_from=0,
            sec_to=0
        )

        with open(filename + ".json", "w") as fw:
            json.dump(metadump, fw, indent=1)


if __name__ == "__main__":

    if len(sys.argv) > 1:
        main(op="full", filename=sys.argv[1])
        #mem = max(memory_usage(proc=main(op="full", filename=sys.argv[1])))
        #print("Maximum memory used: {0} MiB".format(str(mem)))
    else:
        main(op="qrs")
        #main(op="qrs")
