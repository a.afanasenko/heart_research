# coding: utf-8

import numpy as np
from pyclustering.cluster.dbscan import dbscan


sample = [
    [1.0,   1.0,    1.0],
    [1.1,   0.9,    1.0],
    [3.0,   3.0,    3.0],
    [3.1,   2.9,    3.2],
]


sample = np.array(sample)

# Create DBSCAN algorithm.
dbscan_instance = dbscan(sample, 0.5, 1)
# Start processing by DBSCAN.
dbscan_instance.process()
# Obtain results of clustering.
clusters = dbscan_instance.get_clusters()

print(clusters)

noise = dbscan_instance.get_noise()