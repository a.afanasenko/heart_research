# coding: utf-8

"""
.. module:: util
   :platform: Unix, Windows
   :synopsis: Вспомогательные функции для чтения/записи сигналов ЭКГ
Модуль содержит вспомогательные функции для чтения/записи сигналов ЭКГ
"""

import wfdb
import pydicom
import numpy as np
from EDF import EDFReader


if __name__ == "__main__":

    filename = '/Users/arseniy/SERDECH/data/PHYSIONET/I16'

data, fields = wfdb.rdsamp(filename)
