## Классификация QRS-комплексов

### Интерфейс
    from cardio_clean.qrsclassify import density_classifier

    def density_classifier(metadata, **kwargs):
        """ Группировка QRS-комплексов по подобию
        :param sig: сигнал (многоканальный)
        :param hdr: заголовок сигнала
        :param metadata: метаданные с разметкой QRS-комплексов
        :param kwargs: class_generation: минимальное расстояние между классами
        :return: список классов
        """

Параметр class_generation должен иметь строго положительное вещественное
значение. При значениях, близких к нулю, число обнаруженных групп будет
увеличиваться. Значения больше 1.0 не рекомендованы.
Значение class_generation мало влияет на скорость работы классификатора,
менять его с целью повышения производительности не рекомендовано.

### Предварительные условия
Входные метаданные должны содержать информацию о начале и конце QRS,
зубцах Q, R, S, R', S' (при наличии),
типах комплексов (N, S, V, U)
признаки предварительно обнаруженных артефактов и экстрасистол.

### Алгоритм (кратко)

Все QRS-комплексы (кроме артефактных) разбиваются на группы
в соответствии с типом комплекса - N, S, V, U. Для экстрасистол
формируются две дополнительные группы - желудочковые экстрасистолы VE и
наджелудочковые экстрасистолы SE.
В каждой группе производится разбиение на группы по схожести параметров
комплекса. Для этого используется метод DBSCAN.

### Результаты

Результаты выдаются в виде списка описаний групп

group_data = [groupdesc1, groupdesc2, groupdesc3]

     groupdesc = {
            "type": (string) тип комлпекса в классе.
                 Возможные значения "N", "S", "V", "U", "VE", "SE"
            "id": (int) порядковый номер класса
            "average": (numpy.array) - 2d float array, кардиоцикл для справки
            "count": (int) число комплексов, принадлежащих классу
     }


### ПРИМЕР

Как после выполнения density_classifier построить на одном графике
первые 10 комплексов из группы

    import numpy as np
    from matplotlib import pyplot as plt
    from cardio_clean.metadata import extract_cycle
    from cardio_clean.data_keys import QrsKeys as QK

    n = max(10, groupdesc["count"])

    samples = [qrs for qrs in metadata if qrs[QK.qrs_class_id] == groupdesc[
        "id"]]

    chan = 1

    plt.clf()
    for i in xrange(n):

        y, origin = extract_cycle(sig, fs, samples[i])
        x = np.arange(len(y)) - origin
        plt.plot(x, y[:,chan], "c" if i<n-1 else "k")

    plt.title("Group{}:{} [{}]".format(
        groupdesc["type"],
        groupdesc["id"],
        groupdesc["count"]
    ))
    plt.show()