## Анализ нарушений ритма вида СА и АВ блокады

### Интерфейс
    from cardio_clean.blockades import define_sablock, define_avblock


### Предварительные условия
Входные метаданные должны содержать информацию об RR-интервалах
и признаки экстрасистол.

### Алгоритм обнаружения АВ-блокады

АВ-блокада 1 степени Мобиц-1 обнаруживается, если PQ-интервал превышает
пороговое значение 200 мс.


### Параметры
    mobitz_pq: 0.2          # пороговое значение PQ-интервала для обнаружения AV-блокады
    min_pause: 1.5          # минимальная длительность паузы относительно ожидаемой длительности RR
