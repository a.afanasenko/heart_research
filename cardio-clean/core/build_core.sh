#!/usr/bin/env bash

NAME=dbscan
SONAME=lib${NAME}.so

set -e
# -c : Only run preprocess, compile, and assemble steps
# -o <file> : Write output to <file>
gcc -c -Wall -Werror -fpic kdtree.cpp dbscan.cpp interface.cpp
gcc -shared -o ${SONAME} kdtree.o dbscan.o interface.o
# nm - display name list (symbol table)
nm ${SONAME}