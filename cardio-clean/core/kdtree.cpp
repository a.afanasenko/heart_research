
bool GetNodesInRadius(Node* pt, double dblRadius, int nMinPts,
vector<Node*>& rgpNodesFound)
{
    double pos[2] = {pt->GetLongitude(), pt->GetLatitude()};
    kdres* res = kd_nearest_range(kdTree, pos, dblRadius);
    int number_near = kd_res_size(res);
    if (number_near >= nMinPts)
    {
        while (!kd_res_end(res))
        {
            Node* ptNear = (Node*)kd_res_item(res, pos);
            rgpNodesFound.push_back(ptNear);
            kd_res_next(res);

        }
    }
    kd_res_free(res);
    return number_near >= nMinPts;
}