# coding: utf-8


class PkScanner:

    def __init__(self, modas, thresh):
        self.pos = 0
        self.modas = modas
        self.threshold = thresh

    def get_next(self, start_idx, stop_idx, sig):
        moda = []
        while self.modas[self.pos] < start_idx:
            self.pos += 1
        while self.modas[self.pos] <= stop_idx:
            m = self.modas[self.pos]
            val = sig[m]
            if abs(val) > self.threshold:
                moda.append((m, val))
            self.pos += 1
            if self.pos >= len(self.modas):
                break

        return moda