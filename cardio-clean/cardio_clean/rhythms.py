# coding: utf-8

import numpy as np

from data_keys import QrsKeys as QK


rhythm_signatures = [
    ("sin_norm",  u"Нормальный синусовый ритм"),
    ("sin_tachy", u"Cинусовая тахикардия"),
    ("sin_brady", u"синусовая брадикардия"),
    ("sin_other",  u"Синусовая аритмия"),
    ("pacemaker_migration",  u"Миграция водителя ритма"),
    ("atrial",  u"Предсердный ритм"),
    ("ventricular",  u"Желудочковый ритм"),
    ("VT", u"Желудочковая тахикардия"),   # 100-400 bpm
    ("VTP",  u"Пароксизмальная желудочковая тахикардия"),
    ("av_parox", u"Пароксизмальная AB тахикардия"),
    ("a_parox", u"Пароксизмальная предсердная тахикардия"),
    ("PVC", u"Экстрасистолия"),  # можно убрать - разобрана на подтипы
    ("pause", u"Асистолия"),  # можно убрать - разобрана на подтипы
    ("LBBB", u"Блокада ЛНПГ"),
    ("RBBB", u"Блокада ПНПГ"),
    ("WPW", u"Синдром WPW"),
    ("AFL", u"Трепетание предсердий"),   # 200-400 bpm
    ("VFL", u"Трепетание желудочков"),   # 200-300 bpm
    ("AFB", u"Фибрилляция предсердий"),  # 350-700 bpm
    ("VFB", u"Фибрилляция желудочков"),   # 200-500 bpm
    ("PVC_SSE", u"Единичная наджелудочковая ранняя экстрасистолия"),
    ("PVC_SSI", u"Единичная наджелудочковая вставочная экстрасистолия"),
    ("PVC_SVE", u"Единичная желудочковая ранняя экстрасистолия"),
    ("PVC_SVI", u"Единичная желудочковая вставочная экстрасистолия"),
    ("PVC_CSE", u"Парная наджелудочковая ранняя экстрасистолия"),
    ("PVC_CSI", u"Парная наджелудочковая вставочная экстрасистолия"),
    ("PVC_CVE", u"Парная желудочковая ранняя экстрасистолия"),
    ("PVC_CVI", u"Парная желудочковая вставочная экстрасистолия"),
    ("PVC_GSE", u"Групповая наджелудочковая ранняя экстрасистолия"),
    ("PVC_GSI", u"Групповая наджелудочковая вставочная экстрасистолия"),
    ("PVC_GVE", u"Групповая желудочковая ранняя экстрасистолия"),
    ("PVC_GVI", u"Групповая желудочковая вставочная экстрасистолия"),
    ("SAB_I", u"СА-блокада 1 степени"),  # можно убрать - неразличима на ЭКГ
    ("SAB_II", u"СА-блокада 2 степени"),
    ("SAB_III", u"СА-блокада 3 степени"),
    ("AVB_I", u"АВ-блокада 1 степени"),
    ("AVB_II", u"АВ-блокада 2 степени"),
    ("AVB_III", u"АВ-блокада 3 степени"),
    ("PACED_V", u"Желудочковый кардиостимулятор"),
    ("PACED_A", u"Предсердный кардиостимулятор"),
    ("PACED_D", u"Двухкамерный кардиостимулятор")
]

# Цифровые коды ритмов
rhythm_names = {a: b[0] for a,b in enumerate(rhythm_signatures)}
rhythm_codes = {b[0]: a for a,b in enumerate(rhythm_signatures)}


def is_migration2(pqlen_stat, pmax_stat):
    """
    Выявляем миграцию водителя ритма по болтанке PQ - интервала
    :param metadata_block:
    :param pilot_chan:
    :return: True/False
    """

    pqmean = pqlen_stat.average()
    pqsigma = pqlen_stat.std_dev()

    if pqmean is not None and pqsigma is not None:
        if pqsigma > 0.5 * pqmean:
            return True

    return False


def is_migration(metadata_block, pilot_chan):
    """
    Выявляем миграцию водителя ритма по смене полярности P-зубцов
    :param metadata_block:
    :param pilot_chan:
    :return: True/False
    """
    pq = []
    pm = []
    for x in metadata_block:
        pqest = x[QK.pq_duration][pilot_chan]
        if pqest is not None:
            pq.append(pqest)
        p = x[QK.p_height][pilot_chan]
        if p is not None:
            pm.append(p)

    if len(pq) > 0.5*len(metadata_block):
        if np.std(pq) > 0.5 * np.mean(pq):
            return True

    if len(pm) > 0.5*len(metadata_block):
        if np.std(pm) > 0.5 * np.mean(pm):
            return True

    return False


def is_flutter(qrs):
    pilot_chan = 1 if len(qrs[QK.r_pos]) > 1 else 0
    # 0.3 - пороговое значение корреляции для обнаружения периодичности
    return qrs[QK.flutter][pilot_chan] > 0.3


def find_episodes(rythm_marks, min_episode, metadata, signatures=rhythm_signatures):
    rythms = []
    last_r = None
    count = 0
    total_cycles = len(metadata)
    for i, r in enumerate(rythm_marks):
        if i:
            if r == last_r and i < total_cycles-1:
                count += 1
            else:
                if count >= min_episode and last_r >= 0 and last_r != "":

                    start_time = metadata[i-count][QK.qrs_start]
                    end_time = metadata[i-1][QK.qrs_end]

                    desc = last_r if type(last_r) in (str, unicode) else \
                        signatures[last_r][0]

                    rythms.append({
                        "desc": desc,
                        "start": start_time,
                        "end": end_time,
                        "count": count,
                        "modified": False
                    })
                last_r = r
                count = 1
        else:
            last_r = r
            count = 1

    return rythms