# coding: utf-8

from scipy.signal import lfilter, freqz
from metadata import *
from config import config
import sigbind

from matplotlib import pyplot as plt
import numpy as np


def dummy_shift(x, n):
    return np.concatenate((x[n:], np.ones(n)*x[-1]))


def fgraph(b, a):
    w, h = freqz(b, a)
    plt.plot(w/np.pi, np.abs(h))
    plt.show()


def qrs_preprocessing(sig, fs):
    """
    Предварительная обработка для выделения QRS
    :param sig: ЭКС (одноканальный или многоканальный)
    :param fs: частота дискретизации
    :return: характеристическая функция для дальнейшего детектирования
    """
    #TODO: реализовать синтез полосового фильтра 5-15 для произвольной fs
    fse = 250
    fs_err = abs(float(fs-fse))/fse
    if fs_err > 0.05:
        print("WARNING! Частота дискретизации fs={} отличается от "
              "требуемого значения 250 Гц +/-5%".format(fs))

    result = None

    for chan, x in signal_channels(sig):
        # НЧ фильтр (1 - z ^ -6) ^ 2 / (1 - z ^ -1) ^ 2
        b = np.array([1, 0, 0, 0, 0, 0, -2, 0, 0, 0, 0, 0, 1], np.float) / 36
        a = np.array([1, -2, 1], np.float)

        lp = lfilter(b, a, x)
        lp = dummy_shift(lp, 6)

        # слабый ВЧ фильтр z ^ -16 - [(1 - z ^ -32) / (1 - z ^ -1)]
        b = np.array([
            -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 32, -32,
             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1
        ], np.float) / 38.9179
        a = np.array([1, -1], np.float)

        hp = lfilter(b, a, lp)
        hp = dummy_shift(hp, 16)

        # FIXME: правильно инициализировать фильтры
        hp[:19] = 0

        # еще один ВЧ фильтр (производная)
        h = np.array([-1, -2, 0, 2, 1], np.float) / 8

        # сразу возводим в квадрат
        de = lfilter(h, [1.0], hp)**2
        de = dummy_shift(de, 2)

        # усреднение
        sm = lfilter(np.ones(31), [1.0], de)
        sm = dummy_shift(sm, 15)

        # решающие статистики для всех каналов просто суммируются
        result = sm if result is None else result + sm

    return result  # / max(result)


def qrs_detection(sig, fs, **kwargs):
    """
        Обнаружение QRS-комплексов по алгоритму Пана - Томпкинса
    :param sig: ЭКС (одноканальный или многоканальный)
    :param fs: частота дискретизации
    :param minqrs_s: минимальная длительность QRS-комплекса в секундах
    :param maxqrs_s: максмальная длительность QRS-комплекса в секундах
    :return: qrs_metadata (список найденных комплексов), решающая статистика
    """

    pp = qrs_preprocessing(sig, fs)

    # окно для адаптивного порога
    integration_window = int(config.WAVES["qrs_threshold_integration"]*fs)
    avg_mask = np.ones(integration_window) / integration_window
    ppm = np.convolve(pp, avg_mask, 'same')

    inside = False
    qrs_start = 0

    minqrs_ms = kwargs.get(
        "minqrs_s",
        config.WAVES["qrs_duration_min"]
    )
    maxqrs_ms = kwargs.get(
        "maxqrs_s",
        config.WAVES["qrs_duration_max"]
    )

    qrs_kn = config.WAVES["qrs_threshold"]
    ppm *= qrs_kn

    minqrs_smp = int(minqrs_ms * fs)
    maxqrs_smp = int(maxqrs_ms * fs)

    qrs_metadata = []

    for i, v in enumerate(pp):

        flag = 1 if v > ppm[i] else 0

        if flag and not inside:
            qrs_start = i
            inside = True

        if not flag and inside:
            qrs_end = i
            inside = False
            qrs_len = qrs_end - qrs_start
            if minqrs_smp <= qrs_len <= maxqrs_smp:

                qrs_center = qrs_start + np.argmax(
                        pp[qrs_start:qrs_end])

                numch = sig.shape[1] if sig.ndim > 1 else 1
                qrs = metadata_new(numch)
                qrs[QK.qrs_start] = float(qrs_start) / fs
                qrs[QK.qrs_end] = float(qrs_end) / fs
                qrs[QK.qrs_center] = float(qrs_center) / fs

                qrs_metadata.append(qrs)

    return qrs_metadata


def qrs_detection_dev(sig, fs):
    """
        Обнаружение QRS-комплексов по алгоритму Пана - Томпкинса
    :param sig: ЭКС (одноканальный или многоканальный)
    :param fs: частота дискретизации
    :return: qrs_metadata (список найденных комплексов), решающая статистика
    """

    pp = qrs_preprocessing(sig, fs)

    # окно для адаптивного порога
    integration_window = int(config.WAVES["qrs_threshold_integration"]*fs)
    avg_mask = np.ones(integration_window) / integration_window
    ppm = np.convolve(pp, avg_mask, 'same')

    inside = False
    qrs_start = 0

    minqrs_ms = config.WAVES["qrs_duration_min"]
    maxqrs_ms = config.WAVES["qrs_duration_max"]
    qrs_kn = config.WAVES["qrs_threshold"]
    
    ppm *= qrs_kn

    minqrs_smp = int(minqrs_ms * fs)
    maxqrs_smp = int(maxqrs_ms * fs)

    qrs_metadata = []

    for i, v in enumerate(pp):

        flag = 1 if v > ppm[i] else 0

        if flag and not inside:
            qrs_start = i
            inside = True

        if not flag and inside:
            qrs_end = i
            inside = False
            qrs_len = qrs_end - qrs_start
            if minqrs_smp <= qrs_len <= maxqrs_smp:

                qrs_center = qrs_start + np.argmax(
                        pp[qrs_start:qrs_end])

                numch = sig.shape[1] if sig.ndim > 1 else 1
                qrs = metadata_new(numch)
                qrs[QK.qrs_start] = float(qrs_start) / fs
                qrs[QK.qrs_end] = float(qrs_end) / fs
                qrs[QK.qrs_center] = float(qrs_center) / fs

                qrs_metadata.append(qrs)

    return qrs_metadata, pp
