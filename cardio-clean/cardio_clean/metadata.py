# coding: utf-8

import numpy as np
from scipy.stats import linregress

from cardio_clean.util import signal_channels, signal_slice
from cardio_clean.qrsutil import *
from cardio_clean.config import config
from cardio_clean.data_keys import QrsKeys as QK

"""
Метаданные подразделяются на первичные и вторичные.
Первичные являются результатом автоматической сегментации и могут быть
скорректированы вручную. Для расчета вторичных данных используются как
первичные, так и сам сигнал. После ручного редактирования необходимо
пересчитывать вторичные данные, не проводя повторной сегментации сигнала.
"""


def metadata_new(num_channels):
    new_list = [None] * 44

    # Формирование нового массива
    # Неэффективно, только для проверки.
    # qrs-комплекс в целом
    new_list[QK.qrs_start]    = None  # [секунд от начала записи] float
    new_list[QK.qrs_end]      = None  # [секунд от начала записи] float
    new_list[QK.qrs_center]   = None  # [секунд от начала записи] float
    new_list[QK.qrs_class_id] = None  # код класса string
    new_list[QK.flags]        = ""  # string флаги ''(обычный)|'A'(артефакт)|'E'(
    # экстрасистола)|
    new_list[QK.complex_type] = "U"

    # отдельные зубцы
    new_list[QK.p_start]      = [None] * num_channels  # int array
    new_list[QK.p_end]        = [None] * num_channels  # int array
    new_list[QK.p_pos]        = [None] * num_channels  # int array
    new_list[QK.p_height]     = [None] * num_channels  # float array
    new_list[QK.q_pos]        = [None] * num_channels  # int array
    new_list[QK.q_height]     = [None] * num_channels  # float array
    new_list[QK.r_start]      = [None] * num_channels  # int array
    new_list[QK.r_end]        = [None] * num_channels  # int array
    new_list[QK.r_pos]        = [None] * num_channels  # int array
    new_list[QK.r_height]     = [None] * num_channels  # float array
    new_list[QK.r2_pos]       = [None] * num_channels  # int array
    new_list[QK.r2_height]    = [None] * num_channels  # float array
    new_list[QK.s_pos]        = [None] * num_channels  # int array
    new_list[QK.s_height]     = [None] * num_channels  # float array
    new_list[QK.s2_pos]       = [None] * num_channels  # int array
    new_list[QK.s2_height]    = [None] * num_channels  # float array
    new_list[QK.t_start]      = [None] * num_channels  # int array
    new_list[QK.t_end]        = [None] * num_channels  # int array
    new_list[QK.t_pos]        = [None] * num_channels  # int array
    new_list[QK.t_height]     = [None] * num_channels  # float array
    new_list[QK.flutter]      = [0] * num_channels # float array
    new_list[QK.pma]          = [[] for _ in xrange(num_channels)]

    # параметры ритма
    new_list[QK.RR]           = None  # [секунды] float
    new_list[QK.heartrate]    = None  # [удары в минуту] float

    # оценка уровня изолинии
    new_list[QK.isolevel]     = [0]*num_channels  # float array

    # ST-сегмент
    new_list[QK.st_start]       = [None]*num_channels  # int array
    new_list[QK.st_plus]        = [None]*num_channels  # int array
    new_list[QK.st_end]         = [None]*num_channels  # int array
    new_list[QK.st_start_level] = [None]*num_channels  # float array
    new_list[QK.st_plus_level]  = [None]*num_channels  # float array
    new_list[QK.st_end_level]   = [None]*num_channels  # float array
    new_list[QK.st_offset]      = [None]*num_channels  # float array
    new_list[QK.st_duration]    = [None]*num_channels  # float array
    new_list[QK.st_slope]       = [None]*num_channels  # float array

    # QT-интервал
    new_list[QK.qt_duration]    = [None]*num_channels  # float array
    new_list[QK.qtc_duration]   = [None]*num_channels  # float array

    # PQ(R)-интервал
    new_list[QK.pq_duration]    = [None]*num_channels  # float array

    return new_list


def samples_to_sec(smp, fs):
    return float(smp) / fs


def samples_to_ms(smp, fs):
    return smp * 1000.0 / fs


def ms_to_samples(ms, fs):
    return int(ms * fs / 1000.0)


def level_from_pos(d, chan, pos_key, val_key, sig, bias, gain, iso):
    pos = d[pos_key][chan]
    if pos is None:
        d[val_key][chan] = None
    else:
        d[val_key][chan] = ((sig[pos] - bias) / gain) - iso


def erase_qrs(meta, chan):
    """
    Удаляет из метаданных данного комплекса все зубцы, относящиеся к qrs
    :param meta:
    :return:
    """

    meta[QK.q_pos][chan] = None
    meta[QK.q_height][chan] = None
    meta[QK.r_start][chan] = None
    meta[QK.r_end][chan] = None
    meta[QK.r_pos][chan] = None
    meta[QK.r_height][chan] = None
    meta[QK.r2_pos][chan] = None
    meta[QK.r2_height][chan] = None
    meta[QK.s_pos][chan] = None
    meta[QK.s_height][chan] = None
    meta[QK.s2_pos][chan] = None
    meta[QK.s2_height][chan] = None


def is_pvc(cycledata):
    """
    Проверка признака экстрасистолы в данном комплексе
    :param cycledata:
    :return: bool
    """
    return "E" in cycledata[QK.flags]


def is_ve(cycledata):
    """
    Проверка признака ЖЭ в данном комплексе
    :param cycledata:
    :return: bool
    """
    return is_pvc and cycledata[QK.complex_type] == "V"


def set_pvc(cycledata):
    """
    Установка признака ЭС в данном комплексе
    :param cycledata:
    :return: bool
    """
    if "E" not in cycledata[QK.flags]:
        cycledata[QK.flags] += "E"
    # по заданию, экстрасистолы не принадлежат ни к какому классу
    cycledata[QK.qrs_class_id] = None


def is_artifact(cycledata):
    """
    Проверка признака артефакта в данном комплексе
    :param cycledata:
    :return: bool
    """
    return "A" in cycledata[QK.flags]


def set_artifact(cycledata):
    """
    Установка признака артефакта в данном комплексе
    :param cycledata:
    :return: bool
    """
    if "A" not in cycledata[QK.flags]:
        cycledata[QK.flags] += "A"


def reset_artifact(cycledata):
    """
    Сброс признака артефакта в данном комплексе
    :param cycledata:
    :return: bool
    """
    cycledata[QK.flags].replace("A", "")


def safe_r_pos(cycledata):
    heartbeat_channel = 1 if len(cycledata[QK.r_pos]) > 1 else 0

    rz = cycledata[QK.r_pos][heartbeat_channel]
    if rz is None:
        realr = [x for x in cycledata[QK.r_pos] if x is not None]
        if len(realr) > 1:
            rz = np.median(realr)

    if rz is None:
        qz = cycledata[QK.q_pos][heartbeat_channel]
        sz = cycledata[QK.s_pos][heartbeat_channel]
        if qz is not None and sz is not None:
            rz = (qz + sz)/2
    return rz


def get_cycle_start(cycledata, chan, fs):
    """ Возвращает номер отсчета начала цикла

    :param cycledata:
    :param chan:
    :param fs:
    :return: cycle_start
    """
    x = int(cycledata[QK.qrs_start] * fs)
    y = cycledata[QK.p_start][chan]
    if y is not None:
        return min(x, y)

    y = cycledata[QK.p_pos][chan]
    if y is not None:
        return min(x, y)

    y = cycledata[QK.q_pos][chan]
    if y is not None:
        return min(x, y)

    return x


def get_cycle_end(cycledata, chan, fs):
    """ Возвращает номер отсчета конца цикла

    :param cycledata:
    :param chan:
    :param fs:
    :return: cycle_end
    """
    x = int(cycledata[QK.qrs_end] * fs)
    y = cycledata[QK.t_end][chan]
    if y is not None:
        return max(x, y)

    y = cycledata[QK.t_pos][chan]
    if y is not None:
        return max(x, y)

    y = cycledata[QK.st_start][chan]
    if y is not None:
        return max(x, y)

    return x


def extract_cycle(sig, fs, cycledata):

    ch = 1 if len(cycledata[QK.r_pos]) > 1 else 0
    start = get_cycle_start(cycledata, ch, fs)
    stop = get_cycle_end(cycledata, ch, fs)
    center = safe_r_pos(cycledata)

    return signal_slice(sig, start, stop), center-start


def estimate_rr(metadata, pos):
    """
    Оценка RR-интервала
    :param metadata:
    :param pos:
    :return: в отсчетах
    """

    cycledata = metadata[pos]
    num_cycles = len(metadata)

    # Не считаем RR для артефактов
    if is_artifact(cycledata):
        return

    # RR и ЧСС
    rz = safe_r_pos(cycledata)

    if rz is None:
        return

    vice = None
    cand = pos
    if pos < num_cycles - 1:

        while cand < num_cycles - 1:
            cand += 1
            if is_artifact(metadata[cand]):
                continue
            vice = safe_r_pos(metadata[cand])
            if vice is not None:
                break
    else:
        while cand > 1:
            cand -= 1
            if is_artifact(metadata[cand]):
                continue
            vice = safe_r_pos(metadata[cand])
            if vice is not None:
                break

    if vice is not None and vice != rz:
        return float(abs(rz - vice))


def get_true_pp(metadata, pos, fs):
    cycledata = metadata[pos]
    num_cycles = len(metadata)

    # Не считаем RR для артефактов
    if is_artifact(cycledata):
        return

    first = safe_p_pos(cycledata)

    if first is not None:

        second = safe_p_pos(metadata[pos + 1]) if pos < num_cycles - 1 \
            else safe_p_pos(metadata[pos - 1])

        if second is not None:
            return float(abs(first - second)) / fs


def get_true_rr(metadata, pos, fs):


    qrs = metadata[pos]
    num_cycles = len(metadata)

    # Не считаем RR для артефактов и экстрасистол
    if is_artifact(qrs) or is_pvc(qrs):
        return

    first = safe_r_pos(qrs)

    if first is not None:

        if pos < num_cycles - 1:
            pos += 1
        else:
            pos -= 1

        if is_artifact(metadata[pos]) or is_pvc(metadata[pos]):
            return

        second = safe_r_pos(metadata[pos])
        if second is not None:
            return float(abs(first - second)) / fs


def estimate_pp(metadata, pos):
    """

    :param metadata:
    :param pos:
    :return: в отсчетах
    """

    cycledata = metadata[pos]
    num_cycles = len(metadata)

    # Не считаем RR для артефактов
    if is_artifact(cycledata):
        return

    pz = safe_p_pos(cycledata)

    if pz is None:
        return

    vice = None
    cand = pos
    if pos < num_cycles - 1:
        while cand < num_cycles-1:
            cand += 1
            if is_artifact(metadata[cand]):
                continue
            vice = safe_p_pos(metadata[cand])
            if vice is not None:
                break
    else:
        while cand > 1:
            cand -= 1
            if is_artifact(metadata[cand]):
                continue
            vice = safe_p_pos(metadata[cand])
            if vice is not None:
                break

    if vice is not None and vice != pz:
        return float(abs(pz - vice))


def metadata_postprocessing(metadata, sig, header, **kwargs):
    """
    Расчет вторичных параметров сигнала во всех отведениях

    Поскольку источник входных метаданных неизвестен, необходимо
    перезаписать значения всех зависимых ключей.
    :param metadata:
    :param sig:
    :param header: структура с полями fs, adc_gain, baseline
    :param kwargs: параметры j_offset, jplus_offset_ms, min_st_ms,
    qrs_ventricular_min, channel_seq
    :return: None (результатом являются измененные значения в metadata)
    """

    fs = header["fs"]

    j_offset_ms = kwargs.get("j_offset", 60)
    jplus_offset_ms = kwargs.get("jplus_offset", 80)

    ventricular_min_qrs = kwargs.get(
        "qrs_ventricular_min",
        config.WAVES["qrs_ventricular_min"]
    ) * fs

    # число каналов
    numch = sig.shape[1] if sig.ndim == 2 else 1

    # классификация тоже по второму отведению
    classification_channel = 1 if numch > 1 else 0

    #номера комплексов на удаление
    bad_qrs = []

    # определяем соответствие каналов и отведений
    channel_seq = kwargs.get("channel_seq", "std7")

    if type(channel_seq) is str:
        try:
            channel_seq = config.CHANNEL_SEQ[channel_seq]
        except:
            raise(BaseException(
                "Неизвестный стандарт размещения электродов %s" % channel_seq
            ))
    elif type(channel_seq) is list:
        if len(channel_seq) != numch:
            raise(BaseException(
                "Число отведений в выбранном стандарте не совпадает с "
                "реальным числом каналов"
            ))
    else:
        raise (BaseException(
            "Аргумент channel_seq должен иметь тип str или list"
        ))

    # номера каналов, где при нормальном синусовом ритме д/б P > 0
    positive_p_required = [
        channel_seq.index(x) for x in config.RHYTHM["positive_p_in"]
        if x in channel_seq
    ]

    # Удаление выбросов
    for ncycle, qrs in enumerate(metadata):
        delta = ms_to_samples(60, fs)
        remove_outliers(qrs, QK.p_pos, (QK.p_start, QK.p_end), delta)
        remove_outliers(qrs, QK.q_pos, [], delta)
        remove_outliers(qrs, QK.r_pos, (QK.r_start, QK.r_end), delta)
        remove_outliers(qrs, QK.s_pos, [], delta)
        remove_outliers(qrs, QK.t_pos, (QK.t_start, QK.t_end), delta)

    for chan, x in signal_channels(sig):

        for ncycle, qrs in enumerate(metadata):

            if chan == 0:
                # ######################################
                # RR и ЧСС
                rr = estimate_rr(metadata, ncycle)
                if rr is None:
                    set_artifact(qrs)
                    qrs[QK.RR] = None
                    qrs[QK.heartrate] = None
                else:
                    rr /= fs
                    qrs[QK.RR] = rr
                    qrs[QK.heartrate] = 60.0 / rr

            # ######################################
            # точки J и J+
            # ставится со смещением от R-зубца
            rc = qrs[QK.r_pos][chan]
            if rc is not None:
                j_point = rc + ms_to_samples(j_offset_ms, fs)
                # J не может быть раньше конца S или R
                rs_end = qrs[QK.s_pos][chan]
                if rs_end is None:
                    rs_end = qrs[QK.r_end][chan]
                if rs_end is not None:
                    j_point = max(j_point, rs_end)
                if j_point > len(x) - 1:
                    j_point = None
                    jplus_point = None
                else:
                    jplus_point = j_point + ms_to_samples(jplus_offset_ms, fs)
                    if jplus_point > len(x) - 1:
                        jplus_point = None

                    elif qrs[QK.t_start][chan] is not None:
                        tstart = qrs[QK.t_start][chan]
                        jplus_point = min(j_point, tstart)

            else:
                j_point = None
                jplus_point = None

            # ######################################
            # ST
            st_end = qrs[QK.t_start][chan]
            qrs[QK.st_start][chan] = j_point
            qrs[QK.st_plus][chan] = jplus_point
            qrs[QK.st_end][chan] = st_end

            # ######################################
            # запись высоты зубцов
            iso = qrs[QK.isolevel][chan]
            gain = header["adc_gain"][chan]
            dc = header["baseline"][chan]

            level_from_pos(qrs, chan, QK.p_pos, QK.p_height, x, dc, gain, iso)
            level_from_pos(qrs, chan, QK.q_pos, QK.q_height, x, dc, gain, iso)
            level_from_pos(qrs, chan, QK.r_pos, QK.r_height, x, dc, gain, iso)
            level_from_pos(qrs, chan, QK.s_pos, QK.s_height, x, dc, gain, iso)
            level_from_pos(qrs, chan, QK.t_pos, QK.t_height, x, dc, gain, iso)
            level_from_pos(qrs, chan, QK.r2_pos, QK.r2_height, x, dc, gain, iso)
            level_from_pos(qrs, chan, QK.s2_pos, QK.s2_height, x, dc, gain, iso)
            level_from_pos(qrs, chan, QK.st_start,
                           QK.st_start_level, x, dc, gain, iso)
            level_from_pos(qrs, chan, QK.st_plus, QK.st_plus_level, x, dc, gain, iso)
            level_from_pos(qrs, chan, QK.st_end, QK.st_end_level, x, dc, gain, iso)

            # ######################################
            # ST (продолжение)
            if all((j_point, st_end)):
                dur = samples_to_ms(st_end - j_point, fs)
                if dur > kwargs.get("min_st_ms", 40):
                    qrs[QK.st_duration][chan] = dur

                    qrs[QK.st_offset][chan] = (np.mean(
                        sig[j_point:st_end]) - dc) / gain

                    qrs[QK.st_slope][chan] = \
                        (qrs[QK.st_end_level][chan] -
                         qrs[QK.st_start_level][chan]) / \
                        qrs[QK.st_duration][chan]
                else:
                    qrs[QK.st_duration][chan] = None
                    qrs[QK.st_offset][chan] = None
                    qrs[QK.st_slope][chan] = None

            # QT
            qt_start = qrs[QK.q_pos][chan]
            if qt_start is None:
                qt_start = qrs[QK.r_start][chan]

            qt_end = qrs[QK.t_end][chan]

            if qt_start is not None and qt_end is not None:
                qrs[QK.qt_duration][chan] = samples_to_sec(
                    qt_end - qt_start, fs)
            else:
                qrs[QK.qt_duration][chan] = None

            # QTc
            qt = qrs[QK.qt_duration][chan]
            if qt is not None and qrs[QK.RR] is not None:
                qrs[QK.qtc_duration][chan] = qt / np.sqrt(
                    qrs[QK.RR])
            else:
                qrs[QK.qtc_duration][chan] = None

            # PQ(R)
            qrs[QK.pq_duration][chan] = estimate_pq_single(qrs, chan)

            # классификация происходит, когда посчитаны зубцы во всех каналах
            if chan == numch-1:
                # уточняем левую границу QRS
                qrs_start = qrs[QK.q_pos][classification_channel]
                if qrs_start is None:
                    qrs_start = qrs[QK.r_start][classification_channel]

                if qrs_start is not None:
                    qrs_start = float(qrs_start)/fs
                    qrs[QK.qrs_start] = min(qrs_start, qrs[QK.qrs_end])

                # уточняем правую границу QRS
                qrs_end = qrs[QK.s_pos][classification_channel]
                if qrs_end is not None:
                    qrs_end = float(qrs_end)/fs
                    qrs[QK.qrs_end] = max(qrs_end, qrs[QK.qrs_start])

                if qrs[QK.qrs_end] == qrs[QK.qrs_start]:
                   bad_qrs.append(ncycle)

                qrs[QK.complex_type] = define_complex(
                    qrs, fs, classification_channel, ventricular_min_qrs
                )

    for ele in sorted(bad_qrs, reverse=True):
        del metadata[ele]


def define_complex(meta, fs, channel, ventricular_min_qrs):
    """

    :param meta:
    :param channel:
    :param ventricular_min_qrs: мин. длительность желуд. QRS в отсчетах
    :return: N - син. V - желуд. S - наджелуд. U - неизвестный
    """
    qrslen = estimate_qrslen(meta, fs, channel)

    if meta[QK.p_pos][channel] is not None:

        # Предварительная проверка на синусовый ритм
        # R-зубцы присутствуют во всех отведениях, P>0 в отведении  II/V5
        if all(meta[QK.r_pos]) and meta[QK.p_height][channel] > 0:
            return "N"

        # наджелудочковые комплексы - обычные, с P-зубцом
        if qrslen < ventricular_min_qrs:
            return "S"
    else:
        # желудочковые комплексы - широкие, без P, с отриц. T-зубцом (???)
        if qrslen > ventricular_min_qrs:
            return "V"

    return "U"


def safe_p_pos(meta):

    guess = [p for p in meta[QK.p_pos] if p is not None]
    if guess:
        return np.mean(guess)


def calculate_histogram(
        metadata,
        param_name,
        channel,
        bins,
        censoring
):
    """
    Расчет гистограммы значений выбранного параметра в выбранном отведении
    :param metadata: блок метаданных
    :param param_name: имя исследуемого параметра
    :param channel: номер канала или None - считаем по всем каналам
    :param bins: число интервалов в гистограмме
    :param censoring: отбрасывание самых больших и самых маленьких значений
    :return: список элементов гистограммы в виде словарей
            {
                "bin_left",
                "bin_right",
                "count": v,
                "percent"
            }
    """

    if channel is None:
        param_val = [
            x[param_name] for x in metadata if x[param_name] is not None
        ]
    else:
        param_val = [
            x[param_name][channel] for x in metadata if x[param_name][channel] is not
            None
        ]

    # цензурирование - отбрасываем хвосты распределения по 1 проценту
    if censoring:
        q = np.percentile(param_val, [1, 99])
        param_val = [x for x in param_val if q[0] < x < q[1]]

    hist, bin_edges = np.histogram(param_val, bins=bins)

    hdata = []

    for i, v in enumerate(hist):
        hdata.append(
            {
                "bin_left": bin_edges[i],
                "bin_right": bin_edges[i+1],
                "count": v,
                "percent": 100.0 * v / np.sum(hist)
            }
        )

    return hdata


def remove_outliers(metadata, key, dep_keys, delta):
    """
    Удаление далеко отстоящих точек
    :param x: массив значений
    :return: копия входного массива со значениями None для выбросов
    """

    xreal = [i for i in metadata[key] if i is not None]

    # нужно хотя бы три значения
    if len(xreal) < 3:
        return

    m = np.median(xreal)
    #m = np.mean(xreal)

    for i, v in enumerate(metadata[key]):
        if v is not None and abs(v - m) > delta:
            # print("{}[{}]: deviation {} samples".format(
            #     key, i, int(abs(v - m))))

            metadata[key][i] = None
            for k in dep_keys:
                metadata[k][i] = None


def detect_pvc(metadata, look=5, max_rr_s=1.5):
    """
    Поиск экстрасистол
    :param metadata:
    :param look: окрестность для построения модели
    :param max_rr_s: максимально допустимый RR-интервал
    :return: None. Изменяется поле flags в метаданных
    """

    # значащие RR-интервалы
    rr = []
    for i, qrs in enumerate(metadata):
        if not is_artifact(qrs):
            rri = qrs[QK.RR]
            if rri < max_rr_s:
                rr.append((i, qrs[QK.qrs_center], qrs[QK.RR]))

    # предварительно заполенный буфер RR-интервалов
    pack = rr[:look]

    for i, elem in enumerate(rr):

        if i >= len(rr)-1:
            break

        if i > look:
            pack.append(elem)

        if len(pack) > 2*look+1:
            pack.pop(0)

        slope, intercept, r_value, p_value, std_err1 = linregress(
            [x[1] for x in pack],
            [x[2] for x in pack]
        )

        # объединяем последний RR со следующим для проверки ЭС
        pack2 = pack[:]
        last = pack2.pop(-1)
        tst = rr[i+1]
        pack2.append((last[0], last[1], last[2] + tst[2]))

        slope, intercept, r_value, p_value, std_err2 = linregress(
            [x[1] for x in pack2],
            [x[2] for x in pack2]
        )

        if (std_err1 - std_err2) / std_err1 > 0.1:
            #print(tst[1], ["{:.2}".format(x[2]) for x in pack])
            metadata[tst[0]][QK.flags] = "E"