# -*- coding: utf-8 -*-
"""
Модуль для замены словарей на списки.

В случае если индекс через атрибут класса будет слишком медленный, то можно ускорить 
через локализацию вызова перед критичным участком кода, например:

```
from data_keys import QrsKeys as QK
qk_qrs_start = QK.qrs_start
```

Замеры скорости через timeit

Через определение класса атрибутов:
0.0477365000001555

Через namedtuple:
0.11881099999982325

Через enum:
0.14447500000005675
"""

from __future__ import print_function


class QrsKeys(object):
    """
    Перечисление индексов для использования в массиве QRS
    """

    # qrs-комплекс в целом
    qrs_start       =  0  # None,  # [секунд от начала записи] float
    qrs_end         =  1  # None,  # [секунд от начала записи] float
    qrs_center      =  2  # None,  # [секунд от начала записи] float
    qrs_class_id    =  3  # None,  # код класса string
    flags           =  4  # "",  # string флаги ''(обычный)|'A'(артефакт)|'E'(экстрасистола)|
    complex_type    =  5  # "U",
    # отдельные зубцы
    p_start         =  6  # [None]*num_channels,  # int array
    p_end           =  7  # [None]*num_channels,  # int array
    p_pos           =  8  # [None]*num_channels,  # int array
    p_height        =  9  # [None]*num_channels,  # float array
    q_pos           = 10  # [None]*num_channels,  # int array
    q_height        = 11  # [None]*num_channels,  # float array
    r_start         = 12  # [None]*num_channels,  # int array
    r_end           = 13  # [None]*num_channels,  # int array
    r_pos           = 14  # [None]*num_channels,  # int array
    r_height        = 15  # [None]*num_channels,  # float array
    r2_pos          = 16  # [None] * num_channels,  # int array
    r2_height       = 17  # [None] * num_channels,  # float array
    s_pos           = 18  # [None]*num_channels,  # int array
    s_height        = 19  # [None]*num_channels,  # float array
    s2_pos          = 20  # [None] * num_channels,  # int array
    s2_height       = 21  # [None] * num_channels,  # float array
    t_start         = 22  # [None]*num_channels,  # int array
    t_end           = 23  # [None]*num_channels,  # int array
    t_pos           = 24  # [None]*num_channels,  # int array
    t_height        = 25  # [None]*num_channels,  # float array
    flutter         = 27  # [0]*num_channels, # float array
    pma             = 28  # [[] for x in xrange(num_channels)],
    # параметры ритма
    RR              = 29  # None,  # [секунды] float
    heartrate       = 30  # None,  # [удары в минуту] float
    # оценка уровня изолинии
    isolevel        = 31  # [0]*num_channels,  # float array
    # ST-сегмент
    st_start        = 32  # [None]*num_channels,  # int array
    st_plus         = 33  # [None]*num_channels,  # int array
    st_end          = 34  # [None]*num_channels,  # int array
    st_start_level  = 35  # [None]*num_channels,  # float array
    st_plus_level   = 36  # [None]*num_channels,  # float array
    st_end_level    = 37  # [None]*num_channels,  # float array
    st_offset       = 38  # [None]*num_channels,  # float array
    st_duration     = 39  # [None]*num_channels,  # float array
    st_slope        = 40  # [None]*num_channels,  # float array
    # QT-интервал
    qt_duration     = 41  # [None]*num_channels,  # float array
    qtc_duration    = 42  # [None]*num_channels  # float array
    # PQ(R)-интервал
    pq_duration     = 43


class QrsComplexType(object):
    complex_type_N  = 0     # нормальный желудочковый комплекс
    complex_type_V  = 1     # расширенный желудочковый комплекс
    complex_type_S  = 2     # наджелудочковая экстрасистола
    complex_type_U  = 3     # неопределенный (артефакт)