# coding: utf-8

from scipy.signal import find_peaks
from cardio_clean.metadata import *
from cardio_clean.util import signal_channels, quickquantile
from cardio_clean.sigbind import detect_periodic
from cardio_clean.data_keys import QrsKeys as QK
from cardio_clean.peaklib import PkScanner

from data_keys import QrsKeys as QK
from peaklib import PkScanner
from matplotlib import pyplot as plt

def modefind(x, lb=0, rb=0, bias=0.0):

    if rb:
        rb = min(rb, len(x))
    else:
        rb = len(x)

    pk = (0, 0.0)
    for i in xrange(lb, rb):
        val = abs(x[i]-bias)
        if val > pk[1]:
            pk = (i, val)

    return pk


def zerocrossings(x, lb=0, rb=0):

    if rb:
        rb = min(rb, len(x))
    else:
        rb = len(x)

    for i in xrange(lb+1, rb):
        if x[i-1]*x[i] < 0:
            yield i-1 if x[i-1]>=0 else 1-i


def wave_bounds(x, wav, limit):

    w1 = wav[0]
    w2 = wav[1]

    for i in xrange(wav[1], wav[1] + limit):
        if x[i-1]*x[i] < 0:
            w2 = i
            break

    for i in np.arange(limit):
        if x[w1-i] * x[w1-i-1] < 0:
            w1 -= i
            break

    return w1, w2


def fexpand(x):
    """
    Растягивание массива вдвое с добавлением нулей между элементами (
    используется в ДВП)
    :param x: одномерный массив из N элементов
    :return: расширенный массив из 2N элементов
    """
    n = len(x)
    y = np.zeros(2*n, x.dtype)
    for i in xrange(len(y)):
        if i%2 == 0:
            y[i] = x[i/2]

    return y


def ddwt(x, num_scales, store_detail, store_approx=0):
    """
    Дискретное вейвлет-преобразование без прореживания
    :param x: входной сигнал
    :param num_scales: число уровней разложения
    :param store_approx: сколько уровней аппроксимации хранить
    :return:
    """

    if not store_approx:
        store_approx = num_scales

    h = np.array([1, 3, 3, 1], np.float) / 8
    g = np.array([2, -2])
    signal_len = len(x)

    noise_std = 0.0
    detail = []
    approx = []
    ap = x.copy()
    detail.append([])  # на нулевом уровне ничего не храним, для совместимости
    approx.append([])

    for s in xrange(num_scales):
        dly = 2**s
        hif = np.convolve(ap, g, mode="full")[dly:dly+signal_len]

        if s == 0:
            noise_std = np.std(hif)

        if s+1 in store_detail:
            detail.append(hif)
        else:
            detail.append([])

        if s <= store_approx:
            approx.append(ap)

        if s < num_scales-1:
            ap = np.convolve(ap, h, mode="full")[dly:dly+signal_len]
            dly_lo = len(h)-1
            ap[:dly_lo] = ap[dly_lo] # хак
            # вместо прореживания сигналов (Маллат) расширяем характеристики
            # фильтров
            h = fexpand(h)
            g = fexpand(g)

    return approx, detail, noise_std


def detect_all_extrema(modes, sig, smp_from, smp_to, marker):

    ex = []
    idx = -1

    for i, m in enumerate(modes[:-1]):

        x = m[0]
        y = m[1]
        xn = modes[i + 1][0]
        yn = modes[i + 1][1]

        if y*yn < 0:
            if y > yn:
                # find local max
                mpos = x + np.argmax(sig[x:xn])
                mval = sig[mpos]
                mpol = 1
            else:
                # find local min
                mpos = x + np.argmin(sig[x:xn])
                mval = sig[mpos]
                mpol = -1

            if mpos == marker:
                idx = len(ex)

            if smp_from <= mpos <= smp_to:
                # уберем двойные сразу
                if len(ex) and mpol == ex[-1][2]:
                    if mpol > 0:
                        if mval > ex[-1][1]:
                            ex[-1] = (mpos, mval, mpol)
                    else:
                        if mval < ex[-1][1]:
                            ex[-1] = (mpos, mval, mpol)
                else:
                    ex.append((mpos, mval, mpol))

    return ex, idx


def detect_r_pair(modes, smp_from, smp_to, bipol=False):

    maxd = 0      # максимальный перепад
    maxdpos = -1  # номер моды, дающей максимальный перепад
    i = 0
    for x, y in modes[:-1]:

        if smp_from <= x <= smp_to:
            if y * modes[i+1][1] < 0:
                diff = y - modes[i+1][1]

                if bipol:
                    diff = abs(diff)

                if diff > maxd:
                    maxd = diff
                    maxdpos = i
        i += 1

    return maxdpos, maxdpos+1


def qrssearch(modes, tight_bounds, approx, params, chan, isolevel,
              max_qrs_len):
    """

    :param modes: массив с модами производной
    :param tight_bounds: узкие границы для поиска центрального зубца
    :param approx: (сглаженный) входной сигнал
    :param detail:
    :param params: (inout) словарь с параметрами текущего qrs
    :param chan: номер канала
    :param isolevel: уровень изолинии для нахождения начала и конца R-зубца
    :param max_qrs_len: максимальное число отсчетов в QRS
    :return: none
    """

    # убираем все предыдущие значения для QRS
    # FIXME: хотя qrssearch не должен выполняться на уже размеченных сигналах
    erase_qrs(params, chan)

    if len(modes) < 2:
        return

    # Фронты самого крутого положительного зубца (ищем самую мощную пару +-)
    r0, r1 = detect_r_pair(modes, smp_from=tight_bounds[0],
                           smp_to=tight_bounds[1], bipol=False)

    # флаг показывает, есть ли у нас R-зубец
    have_r = r0 >= 0

    if not have_r:
        # не найдено ни одного положительного зубца, ищем отрицательный
        r0, r1 = detect_r_pair(modes, smp_from=tight_bounds[0],
                           smp_to=tight_bounds[1], bipol=True)

        if r0 < 0:
            return

        qs_from = modes[r0][0]
        qs_to = modes[r1][0]
        mpos = qs_from + np.argmin(approx[qs_from:qs_to])
        params[QK.q_pos][chan] = mpos
        params[QK.r_pos][chan] = None # тоже mpos ???
        params[QK.s_pos][chan] = mpos
        return

    # r0 - номер первой моды, соответствующей R-зубцу
    r_from = modes[r0][0]
    r_to = modes[r1][0]
    r_pos = r_from + np.argmax(approx[r_from:r_to])
    params[QK.r_pos][chan] = r_pos

    # уточненные границы комплекса
    qrs_from = r_pos - max_qrs_len/2
    qrs_to = r_pos + max_qrs_len/2

    e, r_idx = detect_all_extrema(modes, approx, qrs_from, qrs_to, r_pos)

    if r_idx < 0:  # ошибка в алгоритме, дальше ничего не выйдет все равно
        return

    q_pos = -1
    r1_pos = -1
    s1_pos = -1
    r2_pos = e[r_idx][0]
    s2_pos = -1

    num_peaks_right = len(e) - r_idx - 1

    # если 0 - значит нет правого S
    if num_peaks_right >= 1:
        if e[r_idx+1][2] < 0:
            s2_pos = e[r_idx+1][0]
        else:
            assert 0  # после перехода на find_peaks не должно быть двойных
            # однополярных пиков

    if num_peaks_right >= 3:
        r1_pos = e[r_idx][0]

        if e[r_idx+1][2] < 0:
            s1_pos = e[r_idx+1][0]
        else:
            assert 0  # после перехода на find_peaks не должно быть двойных
            # однополярных пиков

        if e[r_idx + 2][2] > 0:
            r2_pos = e[r_idx + 2][0]
        else:
            assert 0  # после перехода на find_peaks не должно быть двойных
            # однополярных пиков

        if e[r_idx + 3][2] < 0:
            s2_pos = e[r_idx + 3][0]
        else:
            assert 0  # после перехода на find_peaks не должно быть двойных
            # однополярных пиков

    num_peaks_left = r_idx
    # если 0 - значит нет Q
    if num_peaks_left >= 1:
        r1_pos = e[r_idx][0]
        if e[r_idx - 1][2] < 0:
            q_pos = e[r_idx - 1][0]
        else:
            assert 0  # после перехода на find_peaks не должно быть двойных
            # однополярных пиков

    if num_peaks_left >= 3 and num_peaks_right < 3:
        r2_pos = e[r_idx][0]

        if e[r_idx - 1][2] < 0:
            s1_pos = e[r_idx - 1][0]
        else:
            assert 0  # после перехода на find_peaks не должно быть двойных
            # однополярных пиков

        if e[r_idx - 2][2] > 0:
            r1_pos = e[r_idx - 2][0]
        else:
            assert 0

        if e[r_idx - 3][2] < 0:
            q_pos = e[r_idx - 3][0]
        else:
            assert 0

    if q_pos >= 0:
        params[QK.q_pos][chan] = q_pos

    if r1_pos == r2_pos:
        r2_pos = -1

    if r1_pos >= 0 and r2_pos >= 0:
        rm1 = approx[r1_pos] - isolevel
        rm2 = approx[r2_pos] - isolevel
        rm = max(rm1, rm2)
        t = 0.16
        if rm1 < t * rm:
            r1_pos = -1
        elif rm2 < t * rm:
            r2_pos = -1

    if r1_pos >= 0 and r2_pos >= 0:
        params[QK.r_pos][chan] = min(r1_pos, r2_pos)
        params[QK.r2_pos][chan] = max(r1_pos, r2_pos)
    elif r1_pos >= 0:
        params[QK.r_pos][chan] = r1_pos
    elif r2_pos >= 0:
        params[QK.r_pos][chan] = r2_pos
    else:
        assert 0

    if r2_pos < 0:
        if s1_pos < r_pos:
            s1_pos = -1
        elif s2_pos < r_pos:
            s2_pos = -1

    rmpos = max(r1_pos, r2_pos)
    if s1_pos > rmpos and s2_pos > rmpos:
        s1_pos = min(s1_pos, s2_pos)
        s2_pos = -1

    if s1_pos >= 0 and s2_pos >= 0:

        first_s = min(s1_pos, s2_pos)
        second_s = max(s1_pos, s2_pos)

        params[QK.s_pos][chan] = second_s
        params[QK.s2_pos][chan] = first_s

        # требует уточнения.- должен ли быть основной S всегда глубже
        # удаляем r2 и s2
        if approx[second_s] > approx[first_s]:
            params[QK.s_pos][chan] = first_s
            params[QK.s2_pos][chan] = None
            params[QK.r2_pos][chan] = None

    elif s1_pos >= 0:
        params[QK.s_pos][chan] = s1_pos
    elif s2_pos >= 0:
        params[QK.s_pos][chan] = s2_pos

    rpos = params[QK.r_pos][chan]
    if rpos is not None:
        r_thresh = 0.1 * abs(approx[rpos] - isolevel)
        x = rpos
        while abs(approx[x] - isolevel) > r_thresh:
            if x <= qrs_from:
                break
            x -= 1

        params[QK.r_start][chan] = x
        x = rpos
        while abs(approx[x] - isolevel) > r_thresh:
            x += 1
            if x >= qrs_to:
                break

        params[QK.r_end][chan] = x
    else:
        params[QK.r_start][chan] = None
        params[QK.r_end][chan] = None


def ptsearch(modes, approx, bias, limits, height):
    """
    Поиск зубцов P и T
    :param modes: список экстремумов производной
    :param approx: опорный сигнал для поиска пиков
    :param height: минимальное отклонение от изолинии
    :return: wave_left, wave_center, wave_right
    """

    if len(modes) < 2:
        return None, None, None

    # Фронты самого мощного зубца
    maxpair = (0,0)
    for i, posval in enumerate(modes):
        if i and posval[1]*modes[i - 1][1] < 0:
            diff = abs(posval[1]) + abs(modes[i - 1][1])
            if diff > maxpair[1]:
                maxpair = (i-1, diff)

    i0 = maxpair[0]

    wave_center, ampl = modefind(
        approx, lb=modes[i0][0], rb=modes[i0 + 1][0], bias=bias
    )

    if ampl < height:
        return None, None, None


    # строим касательную в наиболее крутой точке переднего фронта

    x0 = modes[i0][0]
    y0 = approx[x0] - bias
    dy = approx[x0+1] - approx[x0-1]

    if abs(approx[x0+1] - bias) > abs(approx[x0-1] - bias):
        wave_left = int(x0 - 2.0 * y0 / dy)
        if wave_left >= wave_center or wave_left <= limits[0]:
            wave_left = None
    else:
        wave_left = None

    # строим касательную в наиболее крутой точке заднего фронта

    x0 = modes[i0+1][0]
    y0 = approx[x0] - bias
    dy = approx[x0+1] - approx[x0-1]

    if abs(approx[x0+1] - bias) < abs(approx[x0-1] - bias):
        wave_right = int(x0 - 2.0 * y0 / dy)
        if wave_right <= wave_center or wave_right >= limits[1]:
            wave_right = None
    else:
        wave_right = None

    return wave_left, wave_center, wave_right


def _select_extrema(band, modes, start_idx, end_idx, thresh, start_mode=0):

    moda = []
    i = start_mode
    while modes[i] < start_idx:
        i += 1
    while modes[i] <= end_idx:

        val = band[modes[i]]
        if abs(val) > thresh:
            moda.append((modes[i], band[i]))
        i += 1

    return moda


def _find_extrema(band):

    thresh = 1e-10
    # ищем положительные максимумы
    pos = find_peaks(band, height=thresh)[0]
    # ищем отрицательные минимумы
    neg = find_peaks(-band, height=thresh)[0]

    moda = list(pos) + list(neg)
    moda.sort()
    return moda


def find_extrema(band, start_idx, end_idx, thresh):

    moda = []
    # ищем положительные максимумы
    pos = start_idx + find_peaks(band[start_idx:end_idx+1], height=thresh)[0]
    for i in pos:
        moda.append((i, band[i]))
    # ищем отрицательные минимумы
    neg = start_idx + find_peaks(-band[start_idx:end_idx+1], height=thresh)[0]
    for i in neg:
        moda.append((i, band[i]))

    moda.sort()
    return moda


def pma_search(modes, smp_from, smp_to, max_dur):
    if len(modes) < 2:
        return 0

    # Фронты самого крутого положительного зубца (ищем самую мощную пару +-)
    r0, r1 = detect_r_pair(modes, smp_from=smp_from,
                           smp_to=smp_to, bipol=False)

    # флаг показывает, есть ли у нас R-зубец
    have_r = r0 >= 0

    if not have_r:
        # не найдено ни одного положительного зубца, ищем отрицательный
        r0, r1 = detect_r_pair(modes, smp_from=smp_from,
                           smp_to=smp_to, bipol=True)

        if r0 < 0:
            return 0

    dur = modes[r1][0] - modes[r0][0]

    e = 1e-18
    m1 = abs(modes[r1][1])
    m0 = abs(modes[r0][1])
    rat = max(m0, m1) / (min(m0, m1) + e)
    max_rat = 2.0

    if dur < max_dur and rat < max_rat:

        # print(modes[r0][0], modes[r1][0] , dur, abs(rat))
        return int(0.5*(modes[r1][0] + modes[r0][0]))


def estimate_iso(sig, meta, chan, default):

    s = meta[QK.q_pos][chan]
    if s is None:
        s = meta[QK.r_start][chan]
        if s is None:
            s = meta[QK.r_pos][chan]
    ds=8
    if s is not None:
        sm = max(0, s-ds)
        return np.mean(sig[sm:s])

    return default


def find_points(
        sig,
        fs,
        metadata,
        bias,
        gain,
        **kwargs):
    """
        Поиск характерных точек
    :param sig: входной сигнал (многоканальный)
    :param fs: частота дискретизации
    :param metadata: первичная сегментация, содержащая qrs_start, qrs_end,
    qrs_center
    :param bias: уровень изолинии
    :param gain: усиление
    :return: None (результатом являются измененные значения в metadata)
    """

    qrs_duration_max = int(fs*kwargs.get(
        "qrs_duration_max",
        config.WAVES["qrs_duration_max"]
    ))

    pma_detection_on = kwargs.get(
        "pma_detection_on",
        config.PACEMAKER["detection"]
    )
    pma_detection_on = pma_detection_on and fs > config.PACEMAKER["min_fs"]

    pma_duration_max = fs*kwargs.get(
        "pma_duration_max",
        config.PACEMAKER["spike_duration_max"]
    )

    ksigma = kwargs.get(
        "noise_ksigma",
        config.WAVES["noise_ksigma"]
    )

    pma_scale = 1   # номер уровня для поиска артефактов кардиостимулятора
    r_scale = 2     # номер уровня для поиска R-зубца
    pt_scale = 4     # номер уровня для поиска P-зубца,  T-зубца
    f_scale = 4     # номер уровня для обнаружения трепетаний

    t_window_fraction = 0.6
    p_window_fraction = 1.0 - t_window_fraction

    num_scales = max(r_scale, pt_scale, f_scale)
    num_cycles = len(metadata)

    channel_seq = config.SIGNAL["default_channel_sequence"]
    pilot_chan = 0 if sig.ndim == 1 else channel_seq.index("II")

    for chan, x in signal_channels(sig):

        approx, detail, noise_std = ddwt(
            x-bias[chan],
            num_scales=num_scales,
            store_detail={pma_scale, r_scale, pt_scale, f_scale},
            store_approx=r_scale+1
        )

        pscale_extrema = _find_extrema(detail[pt_scale])
        rscale_extrema = _find_extrema(detail[r_scale])

        # границы QRS здесь не определяем, надеемся на metadata

        # очень приближенная оценка шума
        noise = noise_std * ksigma

        pwscan = PkScanner(pscale_extrema, noise/2)
        twscan = PkScanner(pscale_extrema, noise/4)
        rwscan = PkScanner(rscale_extrema, noise/2)

        for ncycle, qrs in enumerate(metadata):

            prev_r = int(metadata[ncycle - 1][QK.qrs_center] * fs) \
                if ncycle else 0
            next_r = int(metadata[ncycle + 1][QK.qrs_center] * fs) \
                if ncycle < num_cycles - 1 else len(x)
            cur_r = int(qrs[QK.qrs_center] * fs)

            # оценка изолинии
            iso = quickquantile(approx[r_scale][prev_r:next_r], 0.15)
            #iso = bias[chan]
            qrs[QK.isolevel][chan] = iso / gain[chan]

            # Поиск зубцов Q, R, S
            # узкое окно для поиска только R
            this_qrs = [int(qrs[QK.qrs_start] * fs), int(qrs[QK.qrs_end] * fs)]

            tight_bounds = [
                max(0, this_qrs[0]),
                min(len(x) - 1, this_qrs[1])
            ]

            # более широкое окно для поиска остальных зубцов
            prev_qrs =\
                int(metadata[ncycle - 1][QK.qrs_end] * fs) if ncycle else 0

            next_qrs =\
                int(metadata[ncycle + 1][QK.qrs_start] * fs) \
                    if ncycle < num_cycles - 1 else len(x)

            loose_bounds = [
                int((tight_bounds[0] + prev_qrs)/2),
                int((tight_bounds[1] + next_qrs)/2)
            ]

            if pma_detection_on:
                pma_modes = find_extrema(
                    detail[pma_scale], loose_bounds[0], loose_bounds[1],
                    noise / 2
                )

                pma = pma_search(pma_modes, tight_bounds[0], tight_bounds[1],
                           max_dur=pma_duration_max)

                if pma:
                    qrs[QK.pma][chan].append(pma)

            if pma_detection_on and fs > 260:
                pma_modes = find_extrema(
                    detail[pma_scale], loose_bounds[0], loose_bounds[1],
                    noise / 2
                )

                pma = pma_search(pma_modes, tight_bounds[0], tight_bounds[1],
                           max_dur=pma_duration_max)

                if pma:
                    qrs[QK.pma][chan].append(pma)

            # все пики производной в широком окне
            modes = rwscan.get_next(loose_bounds[0], loose_bounds[1],
                                           detail[r_scale])

            qrssearch(modes, tight_bounds, approx[r_scale],
                      qrs, chan, iso, qrs_duration_max)



            # поиск P-зубца
            # окно для поиска
            wlen = (cur_r - prev_r) * p_window_fraction

            p_search_lb = int(prev_r + wlen)
            if ncycle:
                prev_t = metadata[ncycle-1][QK.t_end][chan]
                if prev_t is None:
                    prev_t = metadata[ncycle - 1][QK.t_pos][chan]
                if prev_t is not None:
                    p_search_lb = max(p_search_lb,  prev_t)

            pwindow = [
                p_search_lb,
                cur_r
            ]

            modas_subset = pwscan.get_next(pwindow[0], pwindow[1], detail[pt_scale])

            # последняя мода перед R не учитывается, потому что относится к
            #  QRS
            pleft, pcenter, pright = ptsearch(
                modas_subset[:-1],
                approx[r_scale+1],
                bias=iso,
                limits=pwindow,
                height=noise/2
            )

            qrs[QK.p_pos][chan] = pcenter
            qrs[QK.p_start][chan] = pleft
            qrs[QK.p_end][chan] = pright

            # уточнение уровня изолинии по интервалу PQ
            if pright is not None:
                pq_end = qrs[QK.q_pos][chan]
                if pq_end is None:
                    pq_end = qrs[QK.r_start][chan]
                if pq_end is None:
                    pq_end = qrs[QK.r_pos][chan]
                if pq_end is not None and pq_end - pright > 1:
                    iso = np.mean(approx[r_scale][pright:pq_end])
                    qrs[QK.isolevel][chan] = iso / gain[chan]

            # поиск T-зубца
            # окно для поиска
            wlen = (next_r - cur_r) * t_window_fraction
            twindow = [
                cur_r,
                int(cur_r + wlen)
            ]

            modas_subset = twscan.get_next(twindow[0], twindow[1],
                                           detail[pt_scale])

            # первая мода справа от R не учитывается, потому что относится
            # к QRS
            tleft, tcenter, tright = ptsearch(
                modas_subset[1:],
                approx[r_scale+1],
                bias=iso,
                limits=twindow,
                height=noise
            )

            qrs[QK.t_pos][chan] = tcenter
            qrs[QK.t_start][chan] = tleft
            qrs[QK.t_end][chan] = tright

            # поиск F-волн в промежутках между qrs
            if chan == pilot_chan and ncycle and not is_artifact(qrs):

                # Берем промежуток между QRS (с возможным захватом P и T)
                # и обнаруживаем в нем периодичность
                rest_range = [
                    int(metadata[ncycle-1][QK.qrs_end]*fs),
                    int(qrs[QK.qrs_start]*fs)
                    ]

                # защита от слишком коротких пауз
                # TODO: разобраться, почему это происходит
                if rest_range[1] - rest_range[0] > 8:
                    pk = detect_periodic(detail[f_scale][
                                         rest_range[0]:rest_range[1]])
                else:
                    pk = 0

                qrs[QK.flutter][chan] = pk


def explorer(sig, fs, bias):
    for chan, x in signal_channels(sig):

        approx, detail, noise_std = ddwt(
            x-bias[chan],
            num_scales=4,
            store_detail={1, 2, 3, 4},
            store_approx=4
        )

        detail = detail[1:]

        if chan == 2:
            fig, axarr = plt.subplots(1, len(detail), sharex="row")

            for i, s in enumerate(detail):

                axarr[i].plot(s, "k", alpha=0.4)
                axarr[i].plot(x, "k")

                for z in zerocrossings(s):

                    pos = abs(z)
                    col = "r" if z > 0 else "b"

                    axarr[i].scatter(pos, x[pos], c=col)

                axarr[i].grid()
            print("Look at the plots")
            plt.show()