# coding: utf-8

from metadata import *
import numpy as np


def prepare_linplan(npoints):
    """ Расчет матрицы плана эксперимента для линейной регрессии

    :param npoints: число точек
    :return: матрица [npoints X 2]
    """
    p = np.ones((npoints,2))
    for i in xrange(npoints):
        p[i,0] = i

    pp = np.dot(np.transpose(p), p)

    return np.dot(np.linalg.pinv(pp), np.transpose(p))


def calculate_to_ts(rr_buf, pvc_pos, pln, regres_window=4):

    sum_rr_left = rr_buf[pvc_pos-2] + rr_buf[pvc_pos-3]
    sum_rr_right = rr_buf[pvc_pos+2] + rr_buf[pvc_pos+1]
    to = 100.0 * (sum_rr_right - sum_rr_left) / sum_rr_left

    ts = 0
    for i in xrange(pvc_pos+1, len(rr_buf)-regres_window):

        slope = np.dot(pln[0], rr_buf[i:i+regres_window])

        if slope > ts:
            ts = slope

    return to, ts


def turbulence_analyse(metadata, **kwargs):
    """ Анализ турбулентности ритма

    :param metadata: метаданные
    :param kwargs: beats_before, beats_after, "regression_window"
    :return: turbulence_data - данные об эпизодах, отобранных для анализа
             mean_turbulence_data - усредненные показатели турбулентности
    """

    lh = int(kwargs.get(
        "beats_before",
        config.HRT["beats_before"]
    ))
    rh = int(kwargs.get(
        "beats_after",
        config.HRT["beats_after"]
    ))
    tswnd = int(kwargs.get(
        "regression_window",
        config.HRT["regression_window"]
    ))

    linplan = prepare_linplan(tswnd)

    # сразу в мс
    rrbuf = [1000.0*qrs[QK.RR] if qrs[QK.RR] is not None else 0 for qrs in \
            metadata]
    pvc = [is_pvc(qrs) for qrs in metadata]

    turbulence_data = []

    # для тренда
    trend = None
    to_buf = []
    ts_buf = []

    for i, qrs in enumerate(metadata):
        if lh <= i < len(metadata)-rh:
            if pvc[i]:
                #  для анализа нужны фрагменты без других экстрасистол
                # TODO: добавить проверку на тахи/брадикардию
                if sum(pvc[i-lh:i+rh+1]) != 1:
                    continue

                if all(rrbuf[i-lh:i+rh]):
                    to, ts = calculate_to_ts(rrbuf[i-lh:i+rh], lh,
                                             pln=linplan, regres_window=tswnd)
                    turbulence_data.append(
                        {
                            "qrs_index": i,
                            "start_index": -lh + 1,
                            "TO": to,
                            "TS": ts,
                            "curve": rrbuf[i-lh:i+rh]
                        }
                    )
                    # для усреднения
                    to_buf.append(to)
                    ts_buf.append(ts)
                    if trend is None:
                        trend = np.array(rrbuf[i-lh:i+rh])
                    else:
                        trend += rrbuf[i-lh:i+rh]

    if turbulence_data:
        mean_turbulence_data = {
            "start_index": -lh+1,
            "TO": np.mean(to_buf),
            "TS": np.mean(ts_buf),
            "curve": trend / len(turbulence_data)
        }
    else:
        mean_turbulence_data = {
            "start_index": 0,
            "TO": None,
            "TS": None,
            "curve": np.array([])
        }

    return turbulence_data, mean_turbulence_data
