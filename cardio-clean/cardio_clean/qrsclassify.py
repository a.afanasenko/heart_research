# coding: utf-8

"""Классификатор комплексов
"""

import numpy as np
import scipy.stats
from random import shuffle
from cardio_clean.metadata import is_artifact, set_artifact, reset_artifact,\
    is_pvc, extract_cycle
from cardio_clean.config import config
from cardio_clean.util import signal_slice
from cardio_clean.data_keys import QrsKeys as QK
from cardio_clean.metadata import get_cycle_start, get_cycle_end
from matplotlib import pyplot as plt
import scipy.cluster.hierarchy as hcluster
from sklearn.cluster import DBSCAN, MeanShift
from clustering import dbscan


def get_qrs_bounds(meta, fs, pilot_channel):

    left = int(meta[QK.qrs_start] * fs)
    right = int(meta[QK.qrs_end] * fs)

    c = meta[QK.r_pos][pilot_channel]
    if c is None:
        c = int(meta[QK.qrs_center] * fs)
        if c is None:
            c = int((left + right)/2)

    return left, right, c


def extract_qrs(sig, fs, meta, pilot_channel):
    left, right, center = get_qrs_bounds(meta, fs, pilot_channel)
    return signal_slice(sig,left,right), center - left


def _dominant_val(x):
    freq = {}
    for n in x:
        freq[n] = freq.get(n,0) + 1

    for k,v in sorted(freq.items(), key=lambda x: x[1], reverse=True):
        return k


def finalize_classes(qrs_classes, metadata):
    """
        Пост-обработка метаданных и вычисление усредненных комплексов
        помечает артефакты
    :param qrs_classes:
    :param metadata:
    :return:
    """

    artifact_threshold = 1
    classdesc = []
    known_complexes = set()

    # Номера классов - абстрактные, присваиваются исходя из количества
    # экземпляров
    for i, qcl in enumerate(
            sorted(
                qrs_classes,
                key=lambda x: len(x["samples"]),
                reverse=True
            )
    ):
        complex_types = []
        for s in qcl["samples"]:
            complex_types.append(metadata[s][QK.complex_type])
            metadata[s][QK.qrs_class_id] = i

            # помечаем ущербные классы как артефакты
            if len(qcl["samples"]) <= artifact_threshold:
                set_artifact(metadata[s])
            else:
                reset_artifact(metadata[s])
                known_complexes.add(s)

        if len(qcl["samples"]) > artifact_threshold:
            classdesc.append({
                "id": i,
                "average": qcl["accumulator"], #/ len(qcl["samples"]),
                "count": len(qcl["samples"]),
                "type": qcl["type"]
            })

    for i, qrs in enumerate(metadata):
        if i not in known_complexes:
            metadata[i][QK.qrs_class_id] = None
            set_artifact(metadata[i])

    return classdesc


def density_classifier(sig, hdr, metadata, **kwargs):
    """ Группировка QRS-комплексов по подобию
    :param sig: сигнал (многоканальный)
    :param hdr: заголовок сигнала
    :param metadata: метаданные с разметкой QRS-комплексов
    :param kwargs: class_generation: минимальное расстояние между классами
    :return: список классов
    """

    classgen_t = kwargs.get(
        "class_generation",
        config.CLASSIFIER["dbscan_threshold"]
    )

    # для многоканальных сигналов берем одно отведение (2-е стд.)

    num_channels = len(metadata[0][QK.r_pos])

    pilot_chan = config.CLASSIFIER["pilot_channel"] if num_channels > 1 else 0

    qrsgroups = {}
    for i, qrs in enumerate(metadata):

        if is_artifact(qrs):
            continue

        kind = qrs[QK.complex_type]

        if is_pvc(qrs):
            if kind == "V":
                kind = "VE"
            else:
                kind = "SE"

        if kind not in qrsgroups:
            qrsgroups[kind] = {"indices": [], "labels": []}

        qrsgroups[kind]["indices"].append(i)

    classifier = DBSCAN(min_samples=2, eps=classgen_t, algorithm="ball_tree")

    for kind in qrsgroups:

        feature_matrix = []
        for i in qrsgroups[kind]["indices"]:

            r_e = metadata[i][QK.r_end][pilot_chan]
            r_s = metadata[i][QK.r_start][pilot_chan]

            if r_e is not None and r_s is not None:
                rlen = r_e - r_s
            else:
                rlen = None

            feature_matrix.append(
                np.array(
                    [
                        metadata[i][QK.q_height][pilot_chan],
                        metadata[i][QK.r_height][pilot_chan],
                        metadata[i][QK.s_height][pilot_chan],
                        metadata[i][QK.r2_height][pilot_chan],
                        metadata[i][QK.s2_height][pilot_chan],
                        metadata[i][QK.qrs_end] - metadata[i][QK.qrs_start],
                        rlen
                    ],
                    dtype=np.float32
                )
            )

        # нормировка
        feat_std = np.nanstd(feature_matrix, 0)
        feat_std = np.nan_to_num(feat_std) + 1e-10

        feature_matrix = np.nan_to_num(feature_matrix) / feat_std

        # clustering
        clusters_sl = classifier.fit_predict(feature_matrix)

        cld = {}
        for i, c in enumerate(clusters_sl):
            if c < 0:
                continue
            smp = qrsgroups[kind]["indices"][i]
            if c in cld:
                cld[c].append(smp)
            else:
                cld[c] = [smp]

        qrsgroups[kind]["labels"] = cld.copy()

        print("Type {}: {} samples, {} clusters, {} outliers".format(
            kind,
            len(clusters_sl),
            len(cld),
            len([1 for x in clusters_sl if x < 0])
        ))

    return finalize_classes2(sig, hdr["fs"], qrsgroups, metadata)


def finalize_classes2(sig, fs, classdata, metadata):
    """ Переформатирование групп
    :param qrs_classes:
    :param metadata:
    :return:
    """

    artifact_threshold = 1
    classdesc = []

    for kind in classdata:
        for group_id, group_idx in sorted(classdata[kind]["labels"].items(),
                                          key = lambda x: len(x[1]),
                                          reverse=True):

            if group_idx >= 0:
                global_group_id = len(classdesc)

                for i in group_idx:
                    metadata[i][QK.qrs_class_id] = global_group_id
                    reset_artifact(metadata[i])

                classdesc.append({
                    "id": global_group_id,
                    "average": extract_cycle(
                        sig, fs, metadata[group_idx[0]]
                    )[0],
                    "count": len(group_idx),
                    "type": kind
                })
            else:
                # обозначаем артефакт
                for i in group_idx:
                    metadata[i][QK.qrs_class_id] = None
                    set_artifact(metadata[i])

    return classdesc


def incremental_classifier(sig, hdr, metadata, **kwargs):
    """ Однопроходный классификатор QRS-комплексов
    :param sig: сигнал (многоканальный)
    :param hdr: заголовок сигнала
    :param metadata: метаданные с разметкой QRS-комплексов
    :param kwargs: class_generation: нижний порог коэффициента корреляции на
    создание
    нового класса
    :return: список классов
    """

    classgen_t = kwargs.get(
        "class_generation",
        config.CLASSIFIER["class_generation"]
    )

    equivalence = kwargs.get(
        "class_equivalence",
        config.CLASSIFIER["class_equivalence"]
    )
    if equivalence <= classgen_t:
        equivalence = 1.0

    min_qrs = kwargs.get(
        "min_qrs",
        config.WAVES["qrs_duration_min"]
    )

    # число циклов
    num_cyc = len(metadata)

    # для многоканальных сигналов берем одно отведение (2-е стд.)

    pilot_chan = config.CLASSIFIER["pilot_channel"] if sig.ndim > 1 else 0

    if num_cyc < 2:
        print(u"Недостаточно данных для классификации")
        return {}

    fs = hdr["fs"]

    min_len = int(min_qrs*fs)
    first_reference = min(3, num_cyc-1)

    first_qrs, first_c = extract_qrs(sig, fs, metadata[first_reference], pilot_chan)

    # не инициализируем первый класс первым комплексом, т.к. 1-й может быть
    # обрезан
    qrs_classes = [
        {
            "accumulator": first_qrs.copy(),
            "center": first_c,
            "samples": {first_reference},
            "type": metadata[first_reference][QK.complex_type]
        }
    ]

    ccnt = 0

    for i, qrs in enumerate(metadata):

        if is_artifact(qrs):
            continue

        l1, r1, c1 = get_qrs_bounds(qrs, fs, pilot_chan)
        cormat = np.zeros(len(qrs_classes), float)

        cltype = qrs[QK.complex_type]

        for c, qcl in enumerate(qrs_classes):

            if cltype != qcl["type"]:
                continue

            cand_offset = c1 - qcl["center"]
            ref_samples = qcl["accumulator"].shape[0]

            # пропускаем комплексы, присутствующие в сигнале не полностью
            if cand_offset < 0 or cand_offset+ref_samples > sig.shape[0]:
                continue

            if sig.ndim == 1:
                cormat[c] = scipy.stats.pearsonr(
                    qcl["accumulator"],# / len(qcl["samples"]),
                    sig[cand_offset:cand_offset + ref_samples]
                )[0]
            else:
                cormat[c] = scipy.stats.pearsonr(
                    qcl["accumulator"][:, pilot_chan],# / len(qcl["samples"]),
                    sig[cand_offset:cand_offset + ref_samples, pilot_chan]
                )[0]

            ccnt += 1

            if cormat[c] >= equivalence:
                break

        max_cc = np.max(cormat)
        if max_cc < classgen_t:
            # создание нового класса
            new_qrs, new_c = extract_qrs(sig, fs, qrs, pilot_chan)
            if new_qrs.shape[0] >= min_len:
                qrs_classes.append(
                    {
                        "accumulator": new_qrs.copy(),
                        "center": new_c,
                        "samples": {i},
                        "type": cltype
                    }
                )
        else:
            # выбор существуюущего класса
            classid = np.argmax(cormat)
            qcl = qrs_classes[classid]

            # обновление усредненного цикла в выбранном классе
            left_acc = qcl["center"]
            right_acc = qcl["accumulator"].shape[0] - left_acc

            if c1 - left_acc >= 0 and c1 + right_acc <= sig.shape[0]:
                #qcl["accumulator"] += signal_slice(
                #    sig,
                #    c1-left_acc,
                #    c1+right_acc
                #)
                qcl["samples"].add(i)

    # добавляем номера классов в метаданные
    # и формируем сокращенное описание каждого класса

    print("##############")
    print(ccnt)

    return finalize_classes(qrs_classes, metadata)


def new_classifier(sig, hdr, metadata, **kwargs):

    for ncycle, qrs in enumerate(metadata):
        pass
