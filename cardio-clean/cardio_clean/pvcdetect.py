# coding: utf-8

import numpy as np

from cardio_clean.metadata import is_artifact, set_pvc
from cardio_clean.data_keys import QrsKeys as QK
from cardio_clean.config import config


def finalize_group(metadata, emarks, egroup, is_insert):

    # ранняя или вставочная
    atr1 = "I" if is_insert else "E"
    for ncycle in egroup:
        # желудочковая или наджелудочковая
        atr2 = metadata[ncycle][QK.complex_type]
        if atr2 != "V":
            atr2 = "S"

        # одиночная/парная/групповая
        atr3 = "G"
        if len(egroup) == 1:
            atr3 = "S"
        elif len(egroup) == 2:
            atr3 = "C"

        emarks[ncycle] = "PVC_" + atr3 + atr2 + atr1
        set_pvc(metadata[ncycle])


def detect_pvc_episodes(metadata, fs):
    """
    Поиск экстрасистол
    :param metadata:
    :param look: окрестность для построения модели
    :param max_rr_s: максимально допустимый RR-интервал
    :return: None. Изменяется поле flags в метаданных
    """

    # сюда записываем все отметки всех циклов
    emarks = [""] * len(metadata)

    # порог обнаружения начала ЭС относительно нормального интервала RR
    scep = config.PVC["coupling_intl"]

    # общая длительность вставочной ЭС относительно нормального интервала RR
    ins = config.PVC["insertion_dur"]

    # к-т обновления RR по рекуррентной формуле
    k_upd = config.PVC["rr_update"]

    # значащие RR-интервалы
    rr = [x[QK.RR] for x in metadata if not is_artifact(x)]
    # предварительная оценка нормального RR
    rr_e = np.median(rr[:100])*fs
    print(rr_e)
    no_pvc = True
    sdur = 0
    egroup = []

    for i, qrs in enumerate(metadata):
        rr_smp = 0 if is_artifact(qrs) else qrs[QK.RR] * fs
        if no_pvc:
            if rr_smp > 0:
                if rr_smp < scep * rr_e:
                    no_pvc = False
                    sdur = rr_smp
                    egroup = []
                else:
                    rr_e += k_upd*(rr_smp-rr_e)
        else:
            fin = True
            if rr_smp > 0:
                sdur += rr_smp
                egroup.append(i)
                k = 1.05
                fin = sdur > k*rr_e or rr_smp >= scep*rr_e

            if fin:
                finalize_group(
                        metadata,
                        emarks,
                        egroup,
                        is_insert=sdur < ins * rr_e)
                no_pvc = True

    return emarks