#coding: utf-8

"""
.. module:: util
   :platform: Unix, Windows
   :synopsis: Модуль содержит функции обработки сигнала общего назначения
"""


from scipy.signal import hann, argrelmax
import numpy as np
from scipy.fftpack import fft, ifft

from util import signal_channels
from config import config


def mean_spectrum(x, aperture=1024, log_output=True):
    """Расчет усредненного амплитудного спектра

    """
    N = len(x)
    acc = None
    ham = np.array(hann(aperture))

    n1 = 0
    happ = int(aperture/2)
    K = 0
    while n1 < N - aperture:
        xs = np.array(x[n1:n1 + aperture])
        yf = fft(xs * ham)
        yf = np.abs(yf[0:happ])
        K += 1

        if acc is not None:
            acc += yf
        else:
            acc = yf

        n1 += happ

    acc /= K

    if log_output:
        elo = 20000#np.sqrt(sum(np.power(agg_lo, 2)))
        acc = 20.0 * np.log10(acc / elo)

    return acc


def build_comb_filter(fs, n, att, base_freq=50.0, q=5.0):
    """Построение АЧХ гребенчатого режекторного фильтра

    :param fs: частота дискретизации
    :param n: число точек в спектре
    :param att: ослабление гармоник 0 - 1
    :param base_freq: частота первой гармоники
    :param q: ширина полосы задержания
    :return: f_grid, response
    """

    att = min(1.0, max(att, 0.0))
    response = np.ones(n)
    f_grid = np.arange(0.0, fs, float(fs)/n)

    for i, f in enumerate(f_grid):
        real_f = f if f <= fs/2 else fs-f
        for harm in np.arange(base_freq, fs/2, base_freq):
            d = (1.0 - att) * np.exp(-((real_f-harm)/q)**2)
            response[i] = min(response[i], 1.0 - d)

    return f_grid, response


def build_rj_filter(fs, n, att, base_f, q):
    """Построение АЧХ режекторного фильтра

    :param fs: частота дискретизации
    :param n: число точек в спектре
    :param att: ослабление гармоник 0 - 1
    :param base_f: частота первой гармоники
    :param q: ширина полосы задержания
    :return: f_grid, response
    """

    att = min(1.0, max(att, 0.0))
    response = np.ones(n, np.float32)
    f_grid = np.arange(0.0, fs, float(fs)/n)

    for i, f in enumerate(f_grid):
        real_f = f if f <= fs/2 else fs-f
        d = (1.0 - att) * np.exp(-((real_f-base_f)/q)**2)
        response[i] = min(response[i], 1.0 - d)

    return f_grid, response


def build_lp_filter(fs, n, att, freq):
    """Построение АЧХ нч фильтра

    :param fs: частота дискретизации
    :param n: число точек в спектре
    :param att: ослабление гармоник 0 - 1
    :param freq: граница полосы пропускания
    :return: f_grid, response
    """

    att = min(1.0, max(att, 0.0))
    response = np.ones(n, np.float32)
    f_grid = np.arange(0.0, fs, float(fs)/n)

    for i, f in enumerate(f_grid):

        real_f = f if f <= fs/2 else fs-f

        if 0 < real_f <= freq[0]:
            response[i] = 1.0
        elif freq[0] < real_f <= freq[1]:
            k = (real_f - freq[0]) / (freq[1] - freq[0])
            response[i] = 1.0 - k*(1.0 - att)
        else:
            response[i] = att

    return f_grid, response


def fft_filter(sig, transfer_function, bias):
    """Частотная фильтрация

    :param sig: массив отсчетов входного сигнала
    :param transfer_function: массив отсчетов входного сигнала
    :param bias: значение неизменной постоянной составляющей
    :return: массив отсчетов выходного сигнала
    """

    aperture = len(transfer_function)
    y = np.zeros(len(sig), np.float32)
    hamwnd = np.array(hann(aperture))
    step = int(aperture / 2)

    ham_left = hamwnd.copy()
    ham_left[:step + 1] = np.max(hamwnd)

    n2 = len(sig) - aperture
    for n1 in xrange(0, n2, step):
        # для ослабления краевых эффектов берем несимметричное окно в
        # начале
        wnd = ham_left if n1 == 0 else hamwnd
        # комплексный спектр с учетом окна и смещения
        xf = fft(wnd * (np.array(sig[n1:n1 + aperture]) - bias))
        # отфильтрованный сигнал в окне
        yt = np.real(ifft(xf * transfer_function))

        y[n1:n1 + aperture] += yt

    return y + bias


def cardio_filter(sig, fs, bias, mains_attenuation,
                  baseline_attenuation, **kwargs):
    """Частотная фильтрация кардиосигнала

    :param sig: массив отсчетов входного сигнала
    :param fs: частота дискретизации в Гц
    :param bias: значение неизменной постоянной составляющей
    :param mains_attenuation: коэффициент ослабления гармоник сети(0 - полное
    подавление)
    :param baseline_attenuation: коэффициент ослабления дрейфа базовой линии (
    0 - полное подавление)
    :keyword fix_nan: заменять Nan и Inf во входном сигнале
    :keyword mains: частота сети
    :keyword aperture: апертура БПФ
    :return: массив отсчетов выходного сигнала
    """

    fix_nan = kwargs.get(
        "fix_nan",
        config.FILTERING["replace_nan"]
    )

    mains = kwargs.get(
        "mains",
        config.FILTERING["mains_frequency"]
    )

    aperture = kwargs.get(
        "aperture",
        config.FILTERING["fft_aperture"]
    )

    f_grid, fft_response = build_comb_filter(
        fs=fs,
        n=aperture,
        att=mains_attenuation,
        base_freq=mains,
        q=mains*0.03
    )

    if baseline_attenuation < 1:
        f_grid, bsl_response = build_rj_filter(
            fs=fs,
            n=aperture,
            att=baseline_attenuation,
            base_f=0.0,
            q=1.0
        )

        fft_response *= bsl_response

    highfreq_attenuation = config.FILTERING["hf_attenuation"]
    highfreq_cutoff = config.FILTERING["hf_cutoff"]
    if highfreq_attenuation < 1:
        f_grid, lpf_response = build_lp_filter(
            fs=fs,
            n=aperture,
            att=baseline_attenuation,
            freq=highfreq_cutoff
        )

        fft_response *= lpf_response

    if fix_nan:
        np.nan_to_num(sig, False)

    if sig.ndim == 2:
        result = np.zeros(sig.shape, np.float32)

    for chan, x in signal_channels(sig):

        y = np.zeros(len(x), np.float32)
        hamwnd = np.array(hann(aperture))
        step = int(aperture / 2)

        ham_left = hamwnd.copy()
        ham_left[:step+1] = np.max(hamwnd)

        n2 = len(x) - aperture
        for n1 in xrange(0, n2, step):
            # для ослабления краевых эффектов берем несимметричное окно в
            # начале
            wnd = ham_left if n1 == 0 else hamwnd
            # комплексный спектр с учетом окна и смещения
            xf = fft(wnd * (np.array(x[n1:n1 + aperture]) - bias[chan]))
            # отфильтрованный сигнал в окне
            yt = np.real(ifft(xf * fft_response))

            y[n1:n1 + aperture] += yt

        if sig.ndim == 1:
            return np.float32(y + bias[chan])
        else:
            result[:,chan] = np.float32(y + bias[chan])

    return result


def detect_periodic(s):
    """Детектор периодичности на основе измерения амплитуды первого бокового
    всплеска АКФ

    :param s: массив отсчетов сигнала
    :return:
    """
    acf = np.correlate(s, s, mode="same")
    acf = acf / max(acf)
    mid = int(1 + np.floor(len(acf) / 2))
    acf = acf[mid:]
    mx = argrelmax(acf, 0, 10)[0]

    if len(mx) < 1:
        return 0

    pk = [acf[i] for i in mx]

    return max(pk)


def zcfind(x, lb=0, rb=0):

    if rb:
        rb = min(rb, len(x))
    else:
        rb = len(x)

    for i in xrange(lb+1, rb):
        if x[i-1]*x[i] < 0:
            return i-1