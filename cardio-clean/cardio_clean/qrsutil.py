# coding: utf-8

import numpy as np
from cardio_clean.data_keys import QrsKeys as QK


def estimate_pq_single(qrs, chan):
    """ Оценка PQ(R)-интервала в одном канале

    :param qrs: метаданные одного кардиоцикла
    :param chan: номер канала
    :return: длительность интервала в отсчетах
    """
    p = qrs[QK.p_pos][chan]
    if p is None:
        return None
    q_pos = qrs[QK.q_pos][chan]
    if q_pos is not None:
        return q_pos - p
    else:
        r_start = qrs[QK.r_start][chan]
        if r_start is not None:
            return r_start - p
        else:
            r_pos = qrs[QK.r_pos][chan]
            if r_pos is not None:
                return r_pos - p


def estimate_qrslen(meta, fs, chan):
    """
    # Оценка длительности QRS-комплекса по зубцам
    :param meta:
    :param fs:
    :param chan:
    :return:
    """
    lb = int(meta[QK.qrs_start]*fs)
    rb = int(meta[QK.qrs_end]*fs)

    q_left = meta[QK.q_pos][chan]
    if q_left is not None:
        lb = q_left
    else:
        r_left = meta[QK.r_start][chan]
        if r_left is not None:
            lb = r_left

    s_right = meta[QK.s_pos][chan]
    if s_right is not None:
        rb = s_right
    else:
        r_right = meta[QK.r_end][chan]
        if r_right is not None:
            rb = r_right

    return rb - lb