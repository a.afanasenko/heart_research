# coding: utf-8

from random import randint

from cardio_clean.rhythms import *
from cardio_clean.pvcdetect import detect_pvc_episodes
from cardio_clean.blockades import *
from cardio_clean.pmdetect import define_pacemaker_episodes
from cardio_clean.data_keys import QrsKeys as QK
from cardio_clean.interval_timer import IntervalTimer
from cardio_clean.statvariation import RecusiveBufStat

from data_keys import QrsKeys as QK


def arrythmia_name(s):
    """
    Получение человекочитаемого названия для заданного ритма
    :param s: краткое название ритма
    :return: длинное название ритма на русском
    """
    try:
        n = rhythm_signatures.index(s)
        return rhythm_signatures[n][1]
    except:
        return u"Неизвестный ритм " + unicode(s)


def define_rythm(metadata, **kwargs):
    """

    :param metadata:
    :param kwargs: min_episode, fs
    :return:
    """

    min_episode = kwargs.get(
        "min_episode",
        config.RHYTHM["min_episode"]
    )

    fs = kwargs.get(
        "fs",
        config.SIGNAL["default_fs"]
    )

    if not metadata:
        return []

    # для оценки средних показателей используем окно +- wnd циклов
    wnd = 10
    pilot_chan = 1 if len(metadata[0][QK.r_pos]) > 1 else 0

    total_cycles = len(metadata)
    rythm_marks = np.zeros(total_cycles, int)
    # синдромы могут перекрываться с основными ритмами, поэтоу храним отдельно
    # FIXME: хранить все вместе
    syndrome_marks = np.zeros(total_cycles, int) - 1
    flutter_marks = np.zeros(total_cycles, int) - 1

    pqlen_stat = RecusiveBufStat(2*wnd)
    pmax_stat = RecusiveBufStat(2*wnd)
    hrate_stat = RecusiveBufStat(2*wnd)

    for ncycle, qrs in enumerate(metadata):

        if is_lbbb(qrs):
            syndrome_marks[ncycle] = rhythm_codes["LBBB"]
        elif is_rbbb(qrs):
            syndrome_marks[ncycle] = rhythm_codes["RBBB"]

        if ncycle < wnd:
            bnd = [0, 2*wnd]
        elif ncycle > total_cycles-wnd:
            bnd = [total_cycles-2*wnd,total_cycles]
        else:
            bnd = [ncycle-wnd,ncycle+wnd]

        pqlen_stat.add(qrs[QK.pq_duration][pilot_chan])
        pmax_stat.add(qrs[QK.p_height][pilot_chan])
        mig = is_migration2(pqlen_stat, pmax_stat)

        # обнаруживаем миграцию водителя
        if mig:
            rythm_marks[ncycle] = rhythm_codes["pacemaker_migration"]

        # обнаруживаем трепетания во II-м отведении
        if is_flutter(qrs):
            flutter_marks[ncycle] = rhythm_codes["AFL"]

        # ЧСС
        if qrs[QK.heartrate] is not None:
            hrate_stat.add(qrs[QK.heartrate])

        # ???
        avg_hr = hrate_stat.average()
        if avg_hr is None:
            continue

        # Доминирующий тип комплексов
        ct = np.array(
            [x[QK.complex_type] for x in metadata[bnd[0]:bnd[1]] if x[
                QK.complex_type] != "U"]
        )

        dominant = "N"
        #num_sin = len([1 for x in ct if x == "N"])
        num_sup = len([1 for x in ct if x == "S"])
        num_ven = len([1 for x in ct if x == "V"])
        if num_sup > wnd:
            dominant = "S"
        elif num_ven > wnd:
            dominant = "V"

        # постоянный RR-интервал
        r_mark = "sin_norm"

        min_hr = config.RHYTHM["norm_bpm_min"]
        max_hr = config.RHYTHM["norm_bpm_max"]

        std_hr = hrate_stat.std_dev()
        if std_hr is not None and std_hr < 0.1 * avg_hr:
            if min_hr <= avg_hr <= max_hr:
                if dominant == "S":
                    r_mark = "atrial"
                elif dominant == "V":
                    # для желудочкового ритма это уже тахикардия
                    r_mark = "VT"

            elif avg_hr < min_hr:
                if dominant == "V":
                    r_mark = "ventricular"
                else:
                    r_mark = "sin_brady"
            else:
                r_mark = "sin_tachy"
        else:
            if 200 < avg_hr < 300 and dominant == "V":
                r_mark = "VFL"
            else:
                r_mark = "sin_other"

        rythm_marks[ncycle] = rhythm_codes[r_mark]

    tm = IntervalTimer()

    rythms = find_episodes(rythm_marks, min_episode, metadata)
    rythms += find_episodes(syndrome_marks, min_episode, metadata)
    rythms += find_episodes(flutter_marks, min_episode, metadata)

    print("syndromes {:.2f}".format(tm.interval()))

    # сначала выделяем все ЭС
    pvc_marks = detect_pvc_episodes(metadata, fs)
    # потом размечаем их как обычные эпизоды, но с мин. длительностью 1
    rythms += find_episodes(pvc_marks, min_episode=1, metadata=metadata)

    print("PVC {:.2f}".format(tm.interval()))

    # в последнюю очередь ищем паузы
    rythms += define_sablock(metadata)

    print("SA {:.2f}".format(tm.interval()))

    rythms += define_avblock(metadata, fs, min_episode)

    print("AV {:.2f}".format(tm.interval()))

    # и кардиостимулятор
    rythms += define_pacemaker_episodes(metadata)

    print("PMK {:.2f}".format(tm.interval()))

    return rythms


def mock_rythm_episodes(metadata):

    rythms = []

    i = 0
    while i < len(metadata):
        typeid = randint(0, len(rhythm_codes)-1)
        duration = randint(20, 100)
        ending = min(len(metadata)-1, i + duration)

        start_time = metadata[i][QK.qrs_start]
        end_time = metadata[ending][QK.qrs_end]

        rythms.append({
            "desc": rhythm_signatures[typeid],
            "start": start_time,
            "end": end_time,
            "modified": False
        })

        i += duration

    return rythms
