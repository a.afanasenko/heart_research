
Утилита **cardioclean** используется для предварительной обработки
электрокардиосигналов, включающей подавление сетевой помехи и коррекцию
дрейфа изолинии.

*Вызов:*
```
$ cardioclean [-h] [-c \<CONFIG>] -i \<SOURCE> -o \<DEST>
```

- **CONFIG**:
Yaml-файл конфигурации (см. пример в config.yaml)

default: config.yaml в текущем каталоге.

- **SOURCE**:
Путь к исходной записи сигнала (формат MIT). format. Можно указать полный
или относительный путь к файлу .hea или .dat

- **DEST**:
Базовое имя для записи результата. **cardioclean** формирует пару файлов в
текущем каталоге. Например,
```
$ cardioclean ... -o result
```
создаст файлы ./result.dat и ./result.hea.

